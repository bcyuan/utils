﻿using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Neoit.Utils.ThirdParty.NPOI
{
    public class ExcelHelperUse
    {
        public void Run()
        {
            var datas = new List<Stu>();
            datas.Add(new Stu { Name = "jack", Age = 18, Age2 = "18" });
            datas.Add(new Stu { Name = "zed", Age = 19 });

            IWorkbook workbook = new XSSFWorkbook();

            var headerStyle = workbook.CreateCellStyle();
            headerStyle.Alignment = HorizontalAlignment.Center;
            headerStyle.VerticalAlignment = VerticalAlignment.Center;
            headerStyle.WrapText = true;
            headerStyle.FillForegroundColor = HSSFColor.Grey25Percent.Index;
            headerStyle.FillPattern = FillPattern.SolidForeground;
            headerStyle.SetBorderColor(HSSFColor.Grey50Percent.Index);
            headerStyle.SetBorderStyle(BorderStyle.Thin);
            IFont headerFont = workbook.CreateFont();
            headerFont.FontName = "Arial";
            headerFont.FontHeight = 8;
            headerStyle.SetFont(headerFont);

            var columnInfos = new List<ExcelColumnInfo>
            {
                 new ExcelColumnInfo(nameof(Stu.Age2),"年龄2",100),
                new ExcelColumnInfo(nameof(Stu.Name),"姓名"),
                new ExcelColumnInfo(nameof(Stu.Age),"年龄"),
            };

            //byte[] res = ExcelHelper.ExportByCustom(datas, columnInfos, "sheetName");

            //File.WriteAllBytes($"ExcelHelper_{DateTime.Now.ToString("HHmmsss")}.xlsx", res);
        }

    }

    public class Stu
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Age2 { get; set; }
    }
}
