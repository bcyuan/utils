﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Neoit.Utils.ThirdParty.NPOI
{
    public class ExcelHelperPre
    {

        //默认格式的导出
        //public static byte[] Export<T>(List<T> datas, List<ExcelColumnInfo> columnInfos, string sheetName = null)
        //{
        //    IWorkbook workbook = new XSSFWorkbook();
        //    ISheet sheet = workbook.CreateSheet(sheetName ?? "Sheet1");
        //    var PixelRatio = 34;

        //    // header
        //    IRow headerRow = sheet.CreateRow(0);
        //    for (int i = 0; i < columnInfos.Count; i++)
        //    {
        //        var columnInfo = columnInfos[i];
        //        ICell cell = headerRow.CreateCell(i);
        //        cell.SetCellValue(columnInfo.ColumnName);
        //        if (columnInfo.HeaderStyle != null) cell.CellStyle = columnInfo.HeaderStyle;
        //        if (columnInfo.ColumnWidth != null) sheet.SetColumnWidth(i, Convert.ToInt32(columnInfo.ColumnWidth) * PixelRatio);
        //    }

        //    // content
        //    for (int i = 0; i < datas.Count; i++)
        //    {
        //        IRow dataRow = sheet.CreateRow(i + 1);
        //        var dataItem = datas[i];

        //        for (int j = 0; j < columnInfos.Count; j++)
        //        {
        //            var columnInfo = columnInfos[j];
        //            ICell cell = dataRow.CreateCell(j);

        //            var value = typeof(T).GetProperty(columnInfo.PropertyName)?.GetValue(dataItem, null);
        //            cell.SetCellValue(value?.ToString() ?? string.Empty);
        //            if (columnInfo.ContentStyle != null) cell.CellStyle = columnInfo.ContentStyle;
        //        }
        //    }

        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        workbook.Write(stream);
        //        return stream.ToArray();
        //    }
        //}

        //自定义格式的导出
        public static byte[] ExportByCustom<T>(List<T> datas, List<ExcelColumnInfo> columnInfos, string sheetName = null)
        {
            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet(sheetName ?? "Sheet1");
            var PixelRatio = 34;

            var filterTitleStyle = _filterTitleStyle();
            var headerStyle = _headerStyle();
            var contentStyle = _contentStyle();

            // fitler
            for (var i = 0; i < 2; i++)
            {
                var noticeRow = sheet.GetRow(i) ?? sheet.CreateRow(i);
                for (int k = 0; k < columnInfos.Count; k++)
                {
                    var noticeCell = noticeRow.GetCell(k) ?? noticeRow.CreateCell(k);
                    noticeCell.CellStyle = filterTitleStyle;
                }
            }

            // header
            IRow headerRow = sheet.CreateRow(0);
            for (int i = 0; i < columnInfos.Count; i++)
            {
                var columnInfo = columnInfos[i];
                ICell cell = headerRow.CreateCell(i);
                cell.SetCellValue(columnInfo.ColumnName);
                cell.CellStyle = headerStyle;
                if (columnInfo.ColumnWidth != null) sheet.SetColumnWidth(i, Convert.ToInt32(columnInfo.ColumnWidth) * PixelRatio);
            }

            // content
            for (int i = 0; i < datas.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 1);
                var dataItem = datas[i];

                for (int j = 0; j < columnInfos.Count; j++)
                {
                    var columnInfo = columnInfos[j];
                    ICell cell = dataRow.CreateCell(j);

                    var value = typeof(T).GetProperty(columnInfo.PropertyName)?.GetValue(dataItem, null);
                    cell.SetCellValue(value?.ToString() ?? string.Empty);
                    cell.CellStyle = contentStyle;
                }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                workbook.Write(stream);
                return stream.ToArray();
            }

            ICellStyle _filterTitleStyle()
            {
                var style = workbook.CreateCellStyle();
                //创建字体
                XSSFFont ffont = (XSSFFont)workbook.CreateFont();
                //给字体设置颜色
                ffont.Color = HSSFColor.Red.Index;
                ffont.FontName = "Arial"; //和excel里面的字体对应
                ffont.FontHeightInPoints = 8;
                //给样式添加字体
                style.SetFont(ffont);
                //白色边框
                style.BorderBottom = BorderStyle.Thin;
                style.BorderLeft = BorderStyle.Thin;
                style.BorderRight = BorderStyle.Thin;
                style.BorderTop = BorderStyle.Thin;
                style.BottomBorderColor = HSSFColor.White.Index;
                style.LeftBorderColor = HSSFColor.White.Index;
                style.RightBorderColor = HSSFColor.White.Index;
                style.TopBorderColor = HSSFColor.White.Index;

                style.FillForegroundColor = HSSFColor.White.Index;
                style.FillPattern = FillPattern.SolidForeground;

                return style;
            }
            ICellStyle _headerStyle()
            {
                var style = workbook.CreateCellStyle();
                style.Alignment = HorizontalAlignment.Center;
                style.VerticalAlignment = VerticalAlignment.Center;
                style.WrapText = true;
                style.FillForegroundColor = HSSFColor.Grey25Percent.Index;
                style.FillPattern = FillPattern.SolidForeground;
                style.SetBorderColor(HSSFColor.Grey50Percent.Index);
                style.SetBorderStyle(BorderStyle.Thin);
                IFont font = workbook.CreateFont();
                font.FontName = "Arial";
                font.FontHeightInPoints = 8;
                style.SetFont(font);
                return style;
            }
            ICellStyle _contentStyle()
            {
                ICellStyle style = workbook.CreateCellStyle();
                style.VerticalAlignment = VerticalAlignment.Center;
                style.SetBorderColor(HSSFColor.Grey50Percent.Index);
                style.SetBorderStyle(BorderStyle.Thin);
                IFont font = workbook.CreateFont();
                font.FontName = "Arial";
                font.FontHeightInPoints = 8;
                style.SetFont(font);
                return style;
            }
        }

    }

    public static class ExcelExtension
    {
        /* 1. font.FontHeightInPoints(字号) = font.FontHeight * 20
         * 
         */
        public static void SetBorderColor(this ICellStyle cellStyle, short HSSFColorIndex)
        {
            cellStyle.BottomBorderColor = HSSFColorIndex;
            cellStyle.TopBorderColor = HSSFColorIndex;
            cellStyle.LeftBorderColor = HSSFColorIndex;
            cellStyle.RightBorderColor = HSSFColorIndex;
        }
        public static void SetBorderStyle(this ICellStyle cellStyle, BorderStyle style)
        {
            cellStyle.BorderLeft = style;
            cellStyle.BorderRight = style;
            cellStyle.BorderTop = style;
            cellStyle.BorderBottom = style;
        }
    }
}
