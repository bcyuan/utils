﻿namespace Neoit.Utils.ThirdParty.NPOI
{

    public record ExcelColumnInfo(string PropertyName, string ColumnName, int? ColumnWidth = null);

    public class ExcelWriteOption
    {
        public string SheetName { get; set; } = "Sheet1";
        public ExcelHeaderType HeaderType { get; set; }
    }
    public enum ExcelHeaderType
    {
        /// <summary>
        /// 无title
        /// </summary>
        None,
        /// <summary>
        /// 第一条数据
        /// </summary>
        FirstData,
        /// <summary>
        /// 使用MapperInfo中的PropertyName
        /// </summary>
        MapperInfoPropertyName,
        /// <summary>
        /// 使用MapperInfo中的Title
        /// </summary>
        MapperInfoTitle,
        /// <summary>
        /// dynamic中的属性名
        /// </summary>
        dynamicPropertyName
    }
}
