﻿using Neoit.Utils.Office;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Neoit.Utils.ThirdParty.NPOI
{
    public class ExcelHelper
    {
        // 1. read
        #region Read
        public static List<List<string>> Read(string fileName, string sheetName)
        {
            List<List<string>> result = new List<List<string>>();

            using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                IWorkbook workbook;

                // 判断文件格式
                if (fileName.EndsWith(".xlsx"))
                {
                    workbook = new XSSFWorkbook(file);
                }
                else if (fileName.EndsWith(".xls"))
                {
                    workbook = new HSSFWorkbook(file);
                }
                else
                {
                    throw new Exception("文件格式不支持");
                }

                ISheet sheet = workbook.GetSheet(sheetName);
                if (sheet == null)
                {
                    throw new Exception("工作表不存在");
                }

                for (int row = 0; row <= sheet.LastRowNum; row++)
                {
                    IRow currentRow = sheet.GetRow(row);
                    List<string> rowData = new List<string>();

                    if (currentRow != null)
                    {
                        for (int cell = 0; cell < currentRow.LastCellNum; cell++)
                        {
                            ICell currentCell = currentRow.GetCell(cell);
                            rowData.Add(currentCell?.ToString() ?? string.Empty);
                        }
                    }
                    result.Add(rowData);
                }
            }
            return result;
        }
        #endregion

        // 2. create
        #region create
        /// <summary>
        /// 创建excel文件[默认格式]，并返回字节数组；可指定映射信息
        /// 常用case:
        /// 1. 数据源 + 映射信息（按映射顺序+title）
        /// 2. 数据源 + 映射信息
        /// </summary>
        public static byte[] CreateToByte<T>(List<T> datas, List<MapperInfo> mapperInfos = null, ExcelWriteOption option = null)
        {
            option = option ?? new ExcelWriteOption();
            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet(option.SheetName);

            var rowIndex = 0;
            var columnLength = mapperInfos?.Count;
            // header
            if (option.HeaderType != ExcelHeaderType.None)
            {
                IRow headerRow = sheet.CreateRow(rowIndex++);
                if (option.HeaderType == ExcelHeaderType.dynamicPropertyName)
                {
                    var t0 = datas[0];
                    var properties = t0.GetType().GetProperties();
                    columnLength = properties.Length;
                    foreach (var property in properties)
                    {
                        ICell cell = headerRow.CreateCell(properties.ToList().IndexOf(property));
                        cell.SetCellValue(property.Name);
                    }
                }
                else
                {
                    for (int i = 0; i < mapperInfos.Count; i++)
                    {
                        var columnInfo = mapperInfos[i];
                        ICell cell = headerRow.CreateCell(i);
                        switch (option.HeaderType)
                        {
                            case ExcelHeaderType.MapperInfoPropertyName:
                                cell.SetCellValue(columnInfo.PropertyName);
                                break;
                            case ExcelHeaderType.MapperInfoTitle:
                                cell.SetCellValue(columnInfo.Title);
                                break;
                        }
                    }
                }
            }

            // content
            for (int i = 0; i < datas.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(rowIndex++);
                var dataItem = datas[i];

                if (mapperInfos != null && mapperInfos.Count > 0)
                {
                    for (int j = 0; j < mapperInfos.Count; j++)
                    {
                        var columnInfo = mapperInfos[j];
                        ICell cell = dataRow.CreateCell(j);

                        var value = typeof(T).GetProperty(columnInfo.PropertyName)?.GetValue(dataItem, null);
                        cell.SetCellValue(value?.ToString() ?? string.Empty);
                    }
                }
                else
                {
                    var t0 = datas[0];
                    var properties = t0.GetType().GetProperties();
                    for (int j = 0; j < properties.Length; j++)
                    {
                        ICell cell = dataRow.CreateCell(j);
                        var value = properties[j].GetValue(dataItem, null);
                        cell.SetCellValue(value?.ToString() ?? string.Empty);
                    }
                }

            }

            using (MemoryStream stream = new MemoryStream())
            {
                workbook.Write(stream);
                return stream.ToArray();
            }
        }
        #endregion

    }
}
