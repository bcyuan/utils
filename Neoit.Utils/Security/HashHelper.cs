﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Neoit.Utils
{
    /*哈希函数可以把任意长度字符串计算出一个固定长度的字符串(16进制字符串)
     * 常见的有MD5，SHA1，SHA224，SHA384，SHA512
     * 特点：1，算法不可逆 2，极小概率碰撞 3，可穷举字典(最简单的Md5穷举量都是PB级别的)
     * 加盐可以防止穷举破解
     */
    /// <summary>
    /// hash算法
    /// </summary>
    public static class HashHelper
    {
        #region MD5算法
        /// <summary>
        /// 16位MD5加密-小写
        /// </summary>
        public static string MD5_16(string source)
        {
            using var md5 = MD5.Create();
            return BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(source)), 4, 8).Replace("-", "").ToLower();
        }
        /// <summary>
        /// 32位MD5加密-小写
        /// </summary>
        public static string MD5_32(string source)
        {
            using var md5 = MD5.Create();
            byte[] hashValue = md5.ComputeHash(Encoding.UTF8.GetBytes(source));
            return ConvertToHexString(hashValue);
        }
        #endregion

        #region  Sha1 sha256 sha512算法
        /// <summary>
        /// Sha1算法-小写40位
        /// 1：356a192b7913b04c54574d18c28d46e6395428ab
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Sha1(string value)
        {
            using var sha = SHA1.Create();
            byte[] hashValue = sha.ComputeHash(Encoding.UTF8.GetBytes(value));//20字节
            return ConvertToHexString(hashValue);
        }
        /// <summary>
        /// Sha256算法-小写64位
        /// 1：6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Sha256(string source)
        {
            using var sha = SHA256.Create();
            byte[] hashValue = sha.ComputeHash(Encoding.UTF8.GetBytes(source));
            return ConvertToHexString(hashValue);
        }
        /// <summary>
        /// Sha512算法-小写128位
        /// 1：4dff4ea340f0a823f15d3f4f01ab62eae0e5da579ccb851f8db9dfe84c58b2b37b89903a740e1ee172da793a6e79d560e5f7f9bd058a12a280433ed6fa46510a
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Sha512(string source)
        {
            using var sha = SHA512.Create();
            byte[] hashValue = sha.ComputeHash(Encoding.UTF8.GetBytes(source));
            return ConvertToHexString(hashValue);
        }
        #endregion

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        static string ConvertToHexString(byte[] bytes) => BitConverter.ToString(bytes).Replace("-", "").ToLower();
    }
}
