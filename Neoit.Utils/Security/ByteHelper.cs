﻿using System.Text;

namespace Neoit.Utils
{
    /// <summary>
    /// ByteHelper
    /// </summary>
    public static class ByteHelper
    {
        /// <summary>
        /// byte[]转为16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ConvertToHexString(this byte[] bytes)
        {
            //同 BitConverter.ToString(bytes).Replace("-", "").ToLower()
            var sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        /*
         * base64字符串 > 字符串 Encoding.Default.GetString(Convert.FromBase64String(source)) eg:  MQ==：1
         * 字符串 > base64字符串 Convert.ToBase64String(Encoding.Default.GetBytes(source)) eg: 1: MQ==
         */
    }
}
