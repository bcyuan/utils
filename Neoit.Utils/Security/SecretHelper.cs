﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Neoit.Utils
{
    /// <summary>
    /// 密钥生成类
    /// </summary>
    public static class SecretHelper
    {
        #region 水印
        /*
         * 场景：随机生成token，并可快速验证某token是否为平台产生
         */
        /// <summary>
        /// 生成PersonalAccessToken： {data 20位}{signature 32位}=52位
        /// 原理：对随机{data}进行签名获得{signature}，组合{data}{signature}即是完整数据
        /// 参考 azure pat=52位
        /// 示例：1b548a81eca0484ab741kdpb7lup1n5q2y9x5wgia79l5e3w1tbn
        /// </summary>
        /// <returns></returns>
        public static string PersonalAccessToken(string key = "")
        {
            var data = Guid.NewGuid().ToString("N").Substring(0, 20);
            var signature = _patSignature(data, key);
            return $"{data}{signature}";
        }
        //PersonalAccessToken
        /// <summary>
        /// 验证水印
        /// </summary>
        /// <param name="pat"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool VerifyPersonalAccessToken(string pat, string key = "")
        {
            if (string.IsNullOrWhiteSpace(pat) || pat.Length != 52) return false;
            var data = pat.Substring(0, 20);
            var signature = pat.Substring(20, 32);
            return signature == _patSignature(data, key);
        }

        private static string _patSignature(string sourceData, string key)
        {
            //生成64位hash值
            using var hmacSHA256 = new HMACSHA256(Encoding.UTF8.GetBytes(key));
            byte[] signatureBytes = hmacSHA256.ComputeHash(Encoding.UTF8.GetBytes(sourceData));
            return Convert.ToBase64String(signatureBytes).Replace("+", "").Replace("/", "").Replace("=", "").PadRight(32, 'x').ToLower().Substring(0, 32);
        }
        #endregion

    }

}
