﻿using System.Collections.Generic;

namespace Neoit.Utils.DataModel
{
    /// <summary>
    /// 常量
    /// </summary>
    public static class ConstantData
    {
        /// <summary>
        /// 常量：周
        /// </summary>
        public static List<LangInfo> Weeks = new List<LangInfo> {
            new LangInfo{Index=0,Number=0,En="Sunday",EnAbbr="Sun",Cn="日" },
            new LangInfo{Index=1,Number=1,En="Monday",EnAbbr="Mon",Cn="一" },
            new LangInfo{Index=2,Number=2,En="Tuesday",EnAbbr="Tue",Cn="二" },
            new LangInfo{Index=3,Number=3,En="Wednesday",EnAbbr="Wed",Cn="三" },
            new LangInfo{Index=4,Number=4,En="Thursday",EnAbbr="Thu",Cn="四" },
            new LangInfo{Index=5,Number=5,En="Friday",EnAbbr="Fri",Cn="五" },
            new LangInfo{Index=6,Number=6,En="Saturday",EnAbbr="Sat",Cn="六" }
        };
        /// <summary>
        /// 常量：月
        /// </summary>
        public static List<LangInfo> Months = new List<LangInfo> {
            new LangInfo{Index=0,Number=1,En="January",EnAbbr="Jan",Cn="一" },
            new LangInfo{Index=1,Number=2,En="February",EnAbbr="Feb",Cn="二" },
            new LangInfo{Index=2,Number=3,En="March",EnAbbr="Mar",Cn="三" },
            new LangInfo{Index=3,Number=4,En="April",EnAbbr="Apr",Cn="四" },
            new LangInfo{Index=4,Number=5,En="May",EnAbbr="May",Cn="五" },
            new LangInfo{Index=5,Number=6,En="June",EnAbbr="Jun",Cn="六" },
            new LangInfo{Index=6,Number=7,En="July",EnAbbr="Jul",Cn="七" },
            new LangInfo{Index=7,Number=8,En="August",EnAbbr="Aug",Cn="八" },
            new LangInfo{Index=8,Number=9,En="September",EnAbbr="Sep",Cn="九" },
            new LangInfo{Index=9,Number=10,En="October",EnAbbr="Oct",Cn="十" },
            new LangInfo{Index=10,Number=11,En="November",EnAbbr="Nov",Cn="十一" },
            new LangInfo{Index=11,Number=12,En="December",EnAbbr="Dec",Cn="十二" }
        };
    }
    /// <summary>
    /// 语言表述信息
    /// </summary>
    public class LangInfo
    {
        /// <summary>
        /// 索引
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 数值
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 中文表述
        /// </summary>
        public string Cn { get; set; }
        /// <summary>
        /// 英文表述
        /// </summary>
        public string En { get; set; }
        /// <summary>
        /// 英文表述-缩写
        /// </summary>
        public string EnAbbr { get; set; }
    }
}
