﻿namespace Neoit.Utils.DataModel
{
    /// <summary>
    /// Name Value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NameValue<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }
    }
    /// <summary>
    /// Name Value Note
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NameValueNote<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }
        public string Note { get; set; }
    }

    public class NameValue
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
    public class NameStringValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
