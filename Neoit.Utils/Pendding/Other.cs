﻿using System;

namespace Neoit.Utils.Pendding
{
    public class Other
    {
        //失败次数越多，下次执行时忽略的次数越多，达到阈值maxCount时，稳定maxCount次数执行
        public static string DelayIfFalse(string tag, Func<bool> func, int maxCount = 6)
        {
            var p = tag.Split(',');
            var p1 = Convert.ToInt32(p[0]);
            var p2 = Convert.ToInt32(p[1]);

            if (p1 > 0)//跳过并修改标记
            {
                p1--;
            }
            else
            {
                var isSuccess = func();
                if (isSuccess) return "0,0";
                p1 = p2 + 1;
                p2 = p1;
            }
            if (p2 >= maxCount) p2 = maxCount;//达到阈值后稳定maxCount次数执行
            return $"{p1},{p2}";
        }
    }
}
