﻿using Neoit.Utils.Extensions;
using System;
using System.Text.RegularExpressions;

namespace Neoit.Utils
{
    /// <summary>
    /// 常用正则
    /// </summary>
    public static class RegexPattern
    {
        #region 常用标识
        /// <summary>
        /// 11位大陆手机号 (宽松的手机验证,以应对运营商可能的新增号段)
        /// </summary>
        public const string Mobile = @"^1[1-9][0-9]{9}$"; //较严格即目前开放号段@"^1[3456789]\d{9}$"  【C#中 \d 会匹配全角数字】
        /// <summary>
        /// 邮箱 （宽泛的邮箱）
        /// </summary>
        public const string Email = @"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        /// <summary>
        /// 传统邮箱 不包括 xx@zh.企业
        /// </summary>
        public const string EmailTradition = @"[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9.-]+[.][a-zA-Z]+$";
        /// <summary>
        /// 18位大陆身份证 兼容末尾Xx
        /// </summary>
        public const string IdCard = @"^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|[Xx])$";
        /// <summary>
        /// 车牌号
        /// </summary>
        public const string CarNumber = @"^([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1})$";
        /// <summary>
        /// 固定电话【01231234567，0123-12345678，4001234567，400-1234567】
        /// </summary>
        public const string FixedPhone = @"(^((0\d{2,3})-)?(\d{7,8})(-(\d{3,8}))?$)|(^400\d{7}$)";
        /// <summary>
        /// 域名
        /// </summary>
        public const string Domain = @"https?:\/\/([^\/\s]+)";

        #endregion

        #region 数值
        /// <summary>
        /// 数值 decimal case:-0.1，0，1.1，12
        /// </summary>
        public const string Decimal = @"-?\d+(.\d+)?";
        /// <summary>
        /// decimal字符串（或成为有理数rational number）
        /// </summary>
        public static bool IsDecimal(this string val) => val.IsMatch($"^{Decimal}$");
        /// <summary>
        /// 非负数【比如面积0,1.32,...】
        /// </summary>
        public const string NonnegativeNumber = @"\d+(\.\d+)?";
        /// <summary>
        /// 是否为非负数
        /// </summary>
        public static bool IsNonnegativeNumber(this string val) => val.IsMatch($"^{NonnegativeNumber}$");
        /// <summary>
        /// 自然数[即0和正整数]：0,1,2...
        /// </summary>
        public const string NaturalNumber = @"\d+";
        /// <summary>
        /// 是否为自然数[即0和正整数]
        /// </summary>
        public static bool IsNaturalNumber(this string val) => val.IsMatch($"^{NaturalNumber}$");
        /// <summary>
        /// 字符串数值属于指定范围的数值（包含端点）- int
        /// </summary>
        /// <param name="val"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool IsRangeInt(this string val, int min, int max)
        {
            if (Regex.IsMatch(val, @"^-?\d+$"))
            {
                var num = int.Parse(val);
                return num >= min && num <= max;
            }
            return false;
        }
        /// <summary>
        /// 字符串数值属于指定范围的数值（包含端点））- double
        /// </summary>
        /// <param name="val"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool IsRangeDouble(this string val, double min, double max)
        {
            if (Regex.IsMatch(val, $"^{Decimal}$"))
            {
                var num = double.Parse(val);
                return num >= min && num <= max;
            }
            return false;
        }

        #endregion
    }

}
