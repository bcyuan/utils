﻿namespace Neoit.Utils.TemplateParser
{
    /// <summary>
    /// ParseManager
    /// </summary>
    public class ParseManager
    {

        /* 1.变量: ${name}
         * 2.if: <if $='age==18'></if>
         * 3.list: <list $='item in list.xx'> </list>
         */

        /// <summary>
        /// Parse
        /// </summary>
        public static string Parse<T>(string template, T t, string fullPath = null)
        {
            template = ParseList.Parse(template, t);
            template = ParseIf.Parse(template, t);
            template = ParseVar.Parse(template, t);
            template = ParsePart.Parse(template, t, fullPath);
            return template;
        }
    }
}
