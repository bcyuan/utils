﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Neoit.Utils
{
    #region 必填项
    /// <summary>
    /// 必填项://用法：[ValidRequired]、[ValidRequired("custom msg")]
    /// </summary>
    public class ValidRequiredAttribute : ValidationAttribute
    {
        /// <summary>
        /// errorMessage
        /// </summary>
        /// <param name="errorMessage"></param>
        public ValidRequiredAttribute(string errorMessage = null)
        {
            ErrorMessage = errorMessage ?? ErrorMessage;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || (value is string && string.IsNullOrWhiteSpace((string)value))) return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName}是必填项！");
            return ValidationResult.Success;
        }
    }
    #endregion

    #region 正则校验
    /// <summary>
    /// 正则校验；默认非必填：[ValidRegex("\d","格式错误")]；
    /// 必填+正则校验：例1 [ValidRegex("\d","格式错误",true)]；例2 [ValidRegex("\d","格式错误",true,"必填提示")]；
    /// 或者：组合校验[ValidRegex("\d","格式错误"),ValidRequired("必填")]
    /// </summary>
    public class ValidRegexAttribute : ValidationAttribute
    {
        /// <summary>
        /// 正则校验
        /// </summary>
        /// <param name="pattern">pattern</param>
        /// <param name="errorMessage">正则校验提示信息</param>
        /// <param name="isRequired">是否必填</param>
        /// <param name="requiredMessage">必填校验提示信息</param>
        public ValidRegexAttribute(string pattern, string errorMessage = null, bool isRequired = false, string requiredMessage = null)
        {
            _pattern = pattern;
            _isRequired = isRequired;
            ErrorMessage = errorMessage ?? ErrorMessage;
            _requiredMessage = requiredMessage ?? ErrorMessage;
        }
        private string _pattern { get; }
        private bool _isRequired { get; }
        private string _requiredMessage { get; }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //必填项
            if (_isRequired) return new ValidRequiredAttribute(_requiredMessage).GetValidationResult(value, new ValidationContext(new object(), null, null) { DisplayName = validationContext.DisplayName });

            //字符串，且有值，才校验
            if (value is string && !string.IsNullOrEmpty((string)value) && !Regex.IsMatch((string)value, _pattern)) return new ValidationResult(ErrorMessage ?? $"{validationContext.DisplayName}：请输入正确的内容！");
            return ValidationResult.Success;

        }
    }
    #endregion

    //示例
    public class TestStu
    {
        [ValidRequired, DisplayName("主键")]
        public int? Id { get; set; }
        [ValidRequired("必填项")]
        public string Name { get; set; }
        [ValidRegex(@"^\d+$", "xxx格式有误", true, "xx是必填项")]
        public string Name2 { get; set; }
        [ValidRegex(@"^http", "url应以http/s开头")]
        public string Name3 { get; set; }
        [MaxLength(12, ErrorMessage = "")]
        public string Name4 { get; set; }
        /*
         * MaxLength(10,"标题最大长度为10") > 标题最大长度为10
         */
    }

}
