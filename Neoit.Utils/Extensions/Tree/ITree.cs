﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Neoit.Utils.Extensions.Tree
{
    public interface TTreeKey<TKey>
    {
        TKey Id { get; set; }
    }

    #region 带 Children 的树形实体接口
    /// <summary>
    /// 带子级的树形实体接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITreeChildren<T, TKey> : TTreeKey<TKey>
    {
        /// <summary>
        /// 子级
        /// </summary>
        IEnumerable<T> Children { get; set; }
    }
    #endregion

    #region 带 ParentId 的树形实体接口
    /// <summary>
    /// 带 ParentId 的树形实体接口
    /// </summary>
    public interface ITreeParentId<T, TKey> : TTreeKey<TKey>
    {
        /// <summary>
        /// 父级id
        /// </summary>
        public TKey ParentId { get; set; }
    }
    #endregion
}
