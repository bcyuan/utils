﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Neoit.Utils.Extensions
{
    /// <summary>
    /// IQueryableExtension
    /// </summary>
    public static class IQueryableExtension
    {
        #region IQueryable<T>.WhereIF()方法
        /// <summary>
        /// WhereIf
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryable"></param>
        /// <param name="condition"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> queryable, bool condition, Expression<Func<T, bool>> predicate)
        {
            if (condition) queryable = queryable.Where(predicate);

            return queryable;
        }
        #endregion
    }
}
