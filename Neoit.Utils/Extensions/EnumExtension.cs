﻿using System;
using System.Collections.Generic;

namespace Neoit.Utils.Extensions
{
    /// <summary>
    /// 枚举扩展
    /// </summary>
    public static class EnumExtension
    {

        /* 基础枚举方法
         *  var a = Enum.GetName(JobStatus.Disable);
         *  var c = Enum.Parse(typeof(JobStatus), "Disable");
         */

        #region 获取枚举信息
        /// <summary>
        /// 获取枚举信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<EnumInfo> Infos<T>() where T : Enum
        {
            var list = new List<EnumInfo>();
            foreach (var name in Enum.GetNames(typeof(T)))
            {
                var value = (T)Enum.Parse(typeof(T), name);
                list.Add(new EnumInfo
                {
                    Key = value,
                    Value = Convert.ToInt32(value),
                    Name = name,
                    DisplayName = typeof(T).GetField(name).GetDisplayName()
                });
            }
            return list;
        }
        #endregion

        #region Enum类提供方法扩展
        /// <summary>
        /// 根据name，返回对应int?值
        /// </summary>
        public static int? GetKeyByName<T>(string name, bool ignoreCase = true) where T : Enum
        {
            if (Enum.TryParse(typeof(T), name, ignoreCase, out var one))
            {
                return (int)one;
            }
            return null;
        }
        /// <summary>
        /// 根据name，返回对应value值
        /// </summary>
        public static T? GetValueByName<T>(string name, bool ignoreCase = true) where T : struct, Enum
        {
            if (Enum.TryParse(name, ignoreCase, out T value))
            {
                return value;
            }
            return null;
        }
        /// <summary>
        /// 根据枚举值获取Name
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetName(this Enum value)
        {
            return Enum.GetName(value.GetType(), value);
        }
        /// <summary>
        /// 根据枚举值获取DisplayName
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            return field.GetDisplayName();
        }
        #endregion
    }

    #region 枚举信息
    /// <summary>
    /// 枚举信息：如key=999，value=JobStatus.Enable、name=“Enable”，displayName=“启用”
    /// </summary>
    public class EnumInfo
    {
        /// <summary>
        /// 枚举项，如: Enable
        /// </summary>
        public object Key { get; set; }
        //为什么用value? Enum的key才是唯一的，value不唯一。
        /// <summary>
        /// 枚举值，如0
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// 枚举名称 如: "Enable"
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 枚举DisplayName 如: "启用"
        /// </summary>
        public string DisplayName { get; set; }
    }

    //public class EnumInfo<T>
    //{

    //}
    #endregion
}
