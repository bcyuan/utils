﻿using System;
using System.Text;

namespace Neoit.Utils.Extensions
{
    /// <summary>
    /// 其他项扩展
    /// </summary>
    public static class OtherExtension
    {
        #region 组合多级异常信息
        /// <summary>
        /// 组合多级异常信息
        /// </summary>
        /// <param name="e">异常</param>
        /// <param name="separator">separator默认换行符</param>
        /// <returns></returns>
        public static string Combine(this Exception e, string separator = null)
        {
            separator ??= Environment.NewLine;
            StringBuilder sb = new StringBuilder();
            CombineInternal(e, sb, separator);
            return sb.ToString();
        }

        private static void CombineInternal(Exception e, StringBuilder sb, string separator)
        {
            sb.Append(e.Message);
            if (e.InnerException != null)
            {
                sb.Append(separator);
                CombineInternal(e.InnerException, sb, separator);
            }
        }
        #endregion
    }
}
