﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeDemo
{
    public class WorkflowProcessDto
    {
        public WorkflowProcessDto Children { get; set; }
        //branchs
        public string Id { get; set; }
        public string Des { get; set; }
        public long KeyId { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
    }
}
