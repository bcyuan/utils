﻿using Microsoft.Extensions.DependencyInjection;

namespace Neoit.Utils.AspNetCore.Extensions
{
    public static class ServiceCollectionExtension
    {
        //#region AddSwagger
        //public static void AddSwaggerWithAuth(this IServiceCollection services)
        //{
        //    services.AddSwaggerGen(c =>
        //    {
        //        //Bearer 的scheme定义
        //        var securityScheme = new OpenApiSecurityScheme()
        //        {
        //            Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
        //            Name = "Authorization",
        //            //参数添加在头部
        //            In = ParameterLocation.Header,
        //            //使用Authorize头部
        //            Type = SecuritySchemeType.Http,
        //            //内容为以 bearer开头
        //            Scheme = "bearer",
        //            BearerFormat = "JWT"
        //        };

        //        //把所有方法配置为增加bearer头部信息
        //        var securityRequirement = new OpenApiSecurityRequirement
        //            {
        //              {
        //               new OpenApiSecurityScheme
        //               {
        //                   Reference = new OpenApiReference
        //                   {
        //                       Type = ReferenceType.SecurityScheme,
        //                       Id = "bearerAuth"
        //                   }
        //               },
        //               new string[] {}
        //              }
        //            };
        //        //注册到swagger中
        //        c.AddSecurityDefinition("bearerAuth", securityScheme);
        //        c.AddSecurityRequirement(securityRequirement);
        //        c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
        //        //c.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}/{AppDomain.CurrentDomain.FriendlyName}.xml");
        //    });
        //}
        //public static void AddSwagger(this IServiceCollection services)
        //{
        //    services.AddSwaggerGen(c =>
        //    {
        //        //注册到swagger中
        //        c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
        //        c.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}/{AppDomain.CurrentDomain.FriendlyName}.xml");
        //    });
        //}
        //#endregion
    }
}
