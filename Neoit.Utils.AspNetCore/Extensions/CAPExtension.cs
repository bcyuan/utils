﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Neoit.Utils.AspNetCore.Extensions
{
    public static class CAPExtension
    {
        public static void AddCapServer(this IServiceCollection services, ConfigurationManager configuration)
        {
            #region Cap-redis-mysql
            services.AddCap(x =>
            {
                x.UseRedis(configuration["Redis"]);
                x.UseMySql(configuration["Mysql"]);
                x.FailedRetryCount = 3;
                x.FailedThresholdCallback = failed =>
                {
                    Console.WriteLine("fail...");
                };
            });
            #endregion
        }
    }
}
