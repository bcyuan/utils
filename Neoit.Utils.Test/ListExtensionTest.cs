﻿using Neoit.Utils.Extensions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Neoit.Utils.Test
{
    public class ListExtensionTest
    {
        [Fact]
        public void Has()
        {
            Assert.True(new List<int> { 1 }.Has());
            Assert.False(new List<int>().Has());
            List<int> nullv = null;
            Assert.False(nullv.Has());
        }
        [Fact]
        public void EmptyIfNull()
        {
            List<int> list = null;
            Assert.Empty(list.EmptyIfNull());
            Assert.NotEmpty(new List<int> { 1 }.EmptyIfNull());
            string[] arr = null;
            Assert.Empty(arr.EmptyIfNull());
            Assert.NotEmpty(new string[] { "1" }.EmptyIfNull());
        }
        [Fact]
        public void JoinString()
        {
            List<int> list = null;
            Assert.Null(list.JoinString());
            Assert.Null(new List<int> { }.JoinString());
            Assert.Equal("1", new List<int> { 1 }.JoinString());
            Assert.Equal("12", new List<int> { 1, 2 }.JoinString());
            Assert.Equal("1,2,3", new List<int> { 1, 2, 3 }.JoinString(","));
            Assert.Equal("1,2", new List<string> { "1", "2" }.JoinString(","));
        }

        [Fact]
        public void Flatten()
        {
            List<List<int>> list = null;
            Assert.Empty(list.Flatten());
            Assert.Equal(new List<int> { 1, 2, 3, 3 }, new List<List<int>> { new List<int> { 1, 2 }, new List<int> { 3, 3 } }.Flatten());
            Assert.Equal(new List<string> { "1", "2", "3", "3" }, new List<List<string>> { new List<string> { "1", "2" }, new List<string> { "3", "3" } }.Where(s => true).Flatten());
            Assert.Equal(new List<int> { 1, 2 }, new List<List<int>> { new List<int> { 1, 2 }, null }.Flatten());
        }
        [Fact]
        public void DistinctX()
        {
            Assert.Equal(new List<int> { 1, 2 }, new List<int> { 1, 2, 2 }.DistinctX(s => s));
            var source = new List<NameValue> {
                new NameValue{Name="jack",Value="1" },
                new NameValue{Name="petter",Value="2" },
                new NameValue{Name="jack",Value="3" },
            };
            var list = new List<NameValue> { new NameValue { Name = "jack", Value = "1" }, new NameValue { Name = "petter", Value = "2" } };
            Assert.Equal(list.ToJson(), source.DistinctX(s => s.Name).ToJson());
        }
        [Fact]
        public void SortByOrder()
        {
            Assert.Equal(new List<int> { 1, 2 }, new List<int> { 1, 2, 2 }.DistinctX(s => s));
            var source = new List<NameValue> {
                new NameValue{Name="jack",Value="1" },
                new NameValue{Name="petter",Value="2" },
                new NameValue{Name="kitty",Value="1" },
            };
            var result = new List<NameValue> {
                new NameValue{Name="petter",Value="2" },
                new NameValue{Name="kitty",Value="1" },
                new NameValue{Name="jack",Value="1" },
            };
            source.SortByOrder(s => s.Name, new string[] { "petter", "kitty", "jack" });
            Assert.Equal(result.ToJson(), source.ToJson());

            source.SortByOrder(s => s.Value, new string[] { "2", "1" }, s => s.Value, new string[] { "petter", "jack", "kitty" });
            Assert.Equal(result.ToJson(), source.ToJson());
        }
        [Fact]
        public void Next()
        {
            var item1 = new NameValue { Name = "jack", Value = "1" };
            var item2 = new NameValue { Name = "tom", Value = "2" };
            var source = new List<NameValue> {
                item1,
                item2
            };
            Assert.Equal(item2, source.Next(item1));
            Assert.Null(source.Next(item2));

            Assert.Equal(item2, source.Next(s => s.Name == "jack"));
        }

        [Fact]
        public void Pivot()
        {
            var data = new List<List<string>>()
                {
                    new List<string>() { "A2", "B1", "C", "A" },
                    new List<string>() { "A2", "B2", "C", "B" },
                    new List<string>() { "D", "B", "C", "A" }
                };

            var dataResult = new List<List<string>>()
                {
                    new List<string>() { "A2", "A2", "D" },
                    new List<string>() { "B1", "B2", "B"},
                    new List<string>() { "C", "C", "C" },
                    new List<string>() { "A", "B", "A" }
                };

            //DistinctAdjacent
            Assert.Equal(dataResult.ToJson(), data.Pivot().ToJson());
        }

        [Fact]
        public void DistinctAdjacent()
        {
            var data = new List<string>() { "A2", "A2", "A1", "A2" };

            var dataResult = new List<string>() { "A2", "A1", "A2" };

            //DistinctAdjacent
            Assert.Equal(dataResult.ToJson(), data.DistinctAdjacent().ToJson());
        }
    }
}
