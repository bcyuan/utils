﻿using Neoit.Utils.Extensions;
using Xunit;

namespace Neoit.Utils.Test
{
    public class EntityExtensionTest
    {

        [Fact]
        public void TrimForString()
        {
            var nv = new NameValue { Name = " a ", Value = null };
            nv.TrimForString();
            Assert.Equal("a", nv.Name);

            NameValue nvn = null;
            nvn.TrimForString();
            Assert.Null(nvn);
        }
        [Fact]
        public void ForeachString()
        {
            var nv = new NameValue { Name = " a ", Value = null };
            nv.ForeachString(s => s == null ? s : s.Trim());
            nv.ForeachString(s => s + "1");
            Assert.Equal("a1", nv.Name);
            Assert.Equal("1", nv.Value);

            NameValue nvn = null;
            nvn.ForeachString(s => s + "1");
            Assert.Null(nvn);
        }

        [Fact]
        public void Foreach()
        {
            var nv = new NameValue<decimal> { Name = "a", Value = 1 };
            nv.Foreach<NameValue<decimal>, decimal>(s => s + 1);
            Assert.Equal("a", nv.Name);
            Assert.Equal(2, nv.Value);

            var nvnotWrite = new NameValueNotWrite { Name = "a " };
            nvnotWrite.Foreach<NameValueNotWrite, string>(s => s.Trim());
            Assert.Equal("a", nvnotWrite.Name);
            Assert.Equal("b ", nvnotWrite.Value);
        }

        [Fact]
        public void ToDic()
        {
            var nv = new NameValue<int> { Name = "a", Value = 1 };
            var dic = nv.ToDic();
            Assert.Equal("a", dic["Name"]);
            Assert.Equal(1, dic["Value"]);

            NameValue<int> nvn = null;
            var dicn = nvn.ToDic();
            Assert.Empty(dicn);

            NameValue<int> nvn3 = new NameValue<int>();
            var dic3 = nvn3.ToDic();
            Assert.Equal(default, dic3["Name"]);
            Assert.Equal(default(int), dic3["Value"]);
        }

        [Fact]
        public void ToEntity()
        {
            var nv = new NameValue<int> { Name = "a", Value = 1 };
            var dic = nv.ToDic();
            var nvNew = dic.ToEntity<NameValue<int>>();
            Assert.True(nv.EqualsX(nvNew));

            dic = null;
            Assert.Null(dic.ToEntity<NameValue<int>>());

            NameValueStatic.Name = "zs";
            var dic4 = typeof(NameValueStatic).ToDicForStaticClass();
            Assert.Equal("zs", dic4["Name"]);
            Assert.Equal(default(int), dic4["Value"]);
        }

        [Fact]
        public void DeepCopy()
        {
            var nv = new NameValue<decimal> { Name = "a", Value = 1 };
            var nv2 = nv.DeepCopy();
            nv.Name = "b";
            Assert.Equal("a", nv2.Name);
        }


        [Fact]
        public void EqualsX()
        {
            Assert.True(new TestEquals1 { Name = "a" }.EqualsX(new TestEquals2 { Name = "a" }, s => s.Name));
            Assert.False(new TestEquals1 { Name = "a" }.EqualsX(new TestEquals2 { Name = "" }, s => s.Name));
            Assert.False(new TestEquals1 { Name = "" }.EqualsX(new TestEquals2 { }, s => s.Name));
            Assert.False(new TestEquals1 { Name = "a" }.EqualsX(new TestEquals2 { Name = "a" }));

            TestEquals2 null2 = null;
            Assert.False(new TestEquals1 { Name = "a" }.EqualsX(null2, s => s.Name));

            TestEquals1 null1 = null;
            Assert.False(null1.EqualsX(new TestEquals2 { Name = "a" }, s => s.Name));

            Assert.True(new TestEquals1 { }.EqualsX(new TestEquals2 { }, s => s.Name));
            Assert.True(null1.EqualsX(null2));

            Assert.False(new TestEquals1 { Age = "2" }.EqualsX(new TestEquals2 { Age = 2 }, s => s.Age));//类型不同
        }
    }

    public class TestEquals1
    {
        public string Name { get; set; }
        public string Mark { get; set; }
        public string Age { get; set; }
    }


    public class TestEquals2
    {
        public string Name { get; set; }
        public string Note { get; set; }
        public int Age { get; set; }
    }
}
