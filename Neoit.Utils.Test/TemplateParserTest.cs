﻿using Neoit.Utils.TemplateParser;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Xunit;

namespace Neoit.Utils.Test
{
    public class TemplateParserTest
    {
        [Fact]
        public void ParseManagerTest()
        {
            var data = new TestStu
            {
                title = "zs",
                age = 18,
                com = new TestStu.ComInfo { name = "ComInfo.name" },
                List = new List<TestStu.ListInfo> {
                    new TestStu.ListInfo {
                        name = "list1",
                        List2=new List<TestStu.ComInfo> { new TestStu.ComInfo { name="list2Name"} } },
                    new TestStu.ListInfo { name = "list2" } }
            };
            var html = File.ReadAllText("TempateParserData/html.html");

            XmlDocument xmlDoc = new XmlDocument();

            var result = ParseManager.Parse(html, data);
            var expected = @"<!DOCTYPE html>
<html lang=""en"">

<head>
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Document</title>
</head>

<body>
    
        <div>zs</div>
        list2Name
        
        
    
        <div>zs</div>
        
        
        
    
    
    <div>xx</div>
    18
</body>

</html>";

            Assert.Equal(expected, result);
        }

        [Fact]
        public void ParseVarTest()
        {
            var data = new TestStu { title = "zs", age = 18, com = new TestStu.ComInfo { name = "ComInfo.name" }, List = new List<TestStu.ListInfo> { new TestStu.ListInfo { name = "list1" }, new TestStu.ListInfo { name = "list2" } } };
            var html = File.ReadAllText("TempateParserData/var.html");

            var result = ParseVar.Parse(html, data);
            var expected = @"<!DOCTYPE html>

<html lang=""en"" xmlns=""http://www.w3.org/1999/xhtml"">
<head>
    <meta charset=""utf-8"" />
    <title></title>
</head>
<body>
    title: zs
    age: 18
    com.name: ComInfo.name
    list.count: 2
    list[0].name: list1
    list[0].name.length: 5
    Undefined: 
</body>
</html>";

            Assert.Equal(expected, result);
        }

        [Fact]
        public void ParseListTest()
        {
            var data = new TestStu
            {
                title = "zs",
                age = 18,
                com = new TestStu.ComInfo { name = "ComInfo.name" },
                List = new List<TestStu.ListInfo> {
                    new TestStu.ListInfo {
                        name = "list1",
                        List2=new List<TestStu.ComInfo> { new TestStu.ComInfo { name="list2Name"} } },
                    new TestStu.ListInfo { name = "list2" } }
            };
            var html = File.ReadAllText("TempateParserData/list.html");

            XmlDocument xmlDoc = new XmlDocument();

            var result = ParseList.Parse(html, data);
            var expected = @"<!DOCTYPE html>
<html lang=""en"">

<head>
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Document</title>
</head>

<body>
    
        <div>${list[0].name}</div>
        ${list[0].list2[0].name}
    
        <div>${list[1].name}</div>
        
    
    
    <div>xx</div>
</body>

</html>";

            Assert.Equal(expected, result);
        }

        [Fact]
        public void ParseIfTest()
        {
            var data = new TestStu { title = "zs", age = 18, com = new TestStu.ComInfo { name = "ComInfo.name" }, List = new List<TestStu.ListInfo> { new TestStu.ListInfo { name = "list1" }, new TestStu.ListInfo { name = "list2" } } };
            var html = File.ReadAllText("TempateParserData/if.html");

            XmlDocument xmlDoc = new XmlDocument();

            var result = TemplateParser.ParseIf.Parse(html, data);
            var expected = @"<!DOCTYPE html>
<html lang=""en"">

<head>
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Document</title>
</head>

<body>
    
        <div>dsd</div>
        
        999
    

    
    <div>xx</div>
    1
    2
    3
    4
    
    
</body>

</html>";

            Assert.Equal(expected, result);
        }

        [Fact]
        public void ParsePartTest()
        {
            var data = new TestStu { title = "zs", age = 18, com = new TestStu.ComInfo { name = "ComInfo.name" }, List = new List<TestStu.ListInfo> { new TestStu.ListInfo { name = "list1" }, new TestStu.ListInfo { name = "list2" } } };

            var html = File.ReadAllText("TempateParserData/part.html");

            var result = ParsePart.Parse(html, data, Path.GetFullPath("TempateParserData/part.html"));
            var expected = @"<div>1</div>
<div>2</div>
<div>3</div>";

            Assert.Equal(expected, result);
        }
    }


    public class TestStu
    {
        public string title { get; set; }
        public int age { get; set; }
        public ComInfo com { get; set; }
        public List<ListInfo> List { get; set; }


        public class ComInfo
        {
            public string name { get; set; }
        }
        public class ListInfo
        {
            public string name { get; set; }
            public List<ComInfo> List2 { get; set; }
        }
    }
}
