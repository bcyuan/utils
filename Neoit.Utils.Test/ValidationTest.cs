﻿using System.ComponentModel.DataAnnotations;
using Xunit;

namespace Neoit.Utils.Test
{
    public class ValidationTest
    {
        [Theory]
        [InlineData(null, null, "DisplayName是必填项！")]
        [InlineData("", null, "DisplayName是必填项！")]
        [InlineData("", "自定义msg", "自定义msg")]
        [InlineData(1, null, null)]
        public void Test_ValidRequiredAttribute(object val, string customMsg, string resultMessage)
        {
            // Arrange
            var attribute = new ValidRequiredAttribute(customMsg);
            var validationContext = new ValidationContext(new object(), null, null) { DisplayName = "DisplayName" };

            // Act
            var result = attribute.GetValidationResult(val, validationContext);

            // Assert
            Assert.Equal(resultMessage, result?.ErrorMessage);
        }
        [Theory]
        [InlineData("123a", @"^\d+$", null, false, null, "DisplayName：请输入正确的内容！")]
        [InlineData("123a", @"^http", "url应以http/s开头", false, null, "url应以http/s开头")]
        [InlineData("http", @"^http", "", false, null, null)]
        [InlineData("123a", @"^\d+$", "regexMessage", false, null, "regexMessage")]
        [InlineData("", @"^\d+$", "regexMessage", true, null, "regexMessage")]
        [InlineData("", @"\d+", "regexMessage", true, "required", "required")]
        [InlineData("123", @"\d+", "", false, "", null)]
        public void Test_ValidRegexAttribute(object val, string pattern, string errorMessage, bool isRequired, string requiredMessage, string resultMessage)
        {
            // Arrange
            var attribute = new ValidRegexAttribute(pattern, errorMessage, isRequired, requiredMessage);
            var validationContext = new ValidationContext(new object(), null, null) { DisplayName = "DisplayName" };

            // Act
            var result = attribute.GetValidationResult(val, validationContext);

            // Assert
            Assert.Equal(resultMessage, result?.ErrorMessage);
        }

    }
}
