﻿using Xunit;

namespace Neoit.Utils.Test
{
    public class RegexPatternTest
    {
        [Fact]
        public void IsDecimal()
        {
            Assert.True("0".IsDecimal());
            Assert.True("123".IsDecimal());
            Assert.True("-1".IsDecimal());
            Assert.True("0.2".IsDecimal());
            Assert.True("-0.36".IsDecimal());

            Assert.False("a123".IsDecimal());
            Assert.False("".IsDecimal());

        }
        [Fact]
        public void IsNonnegativeNumber()
        {
            Assert.True("0".IsNonnegativeNumber());
            Assert.True("123".IsNonnegativeNumber());
            Assert.True("0.2".IsNonnegativeNumber());

            Assert.False("-0.36".IsNonnegativeNumber());
            Assert.False("-1".IsNonnegativeNumber());
            Assert.False("a123".IsNonnegativeNumber());
            Assert.False("".IsNonnegativeNumber());

        }
        [Fact]
        public void IsNaturalNumber()
        {
            Assert.True("0".IsNaturalNumber());
            Assert.True("123".IsNaturalNumber());

            Assert.False("-1".IsNaturalNumber());
            Assert.False("0.2".IsNaturalNumber());
            Assert.False("-0.36".IsNaturalNumber());
            Assert.False("a123".IsNaturalNumber());
            Assert.False("".IsNaturalNumber());

        }

        [Fact]
        public void IsRangeInt()
        {
            Assert.True("30".IsRangeInt(0,59));

            Assert.False("".IsRangeInt(0,59));
            Assert.False("-888a".IsRangeInt(0,59));
            Assert.False("-".IsRangeInt(0,59));
            Assert.False(".".IsRangeInt(0,59));

            Assert.True("0".IsRangeInt(0,59));
            Assert.True("59".IsRangeInt(0,59));
            Assert.False("60".IsRangeInt(0,59));

            Assert.False("0.1".IsRangeInt(0,1));
        }

        [Fact]
        public void IsRangeDouble()
        {
            Assert.True("30".IsRangeDouble(0, 59));

            Assert.False("".IsRangeDouble(0, 59));
            Assert.False("-888a".IsRangeDouble(0, 59));
            Assert.False("-".IsRangeDouble(0, 59));
            Assert.False(".".IsRangeDouble(0, 59));

            Assert.True("0".IsRangeDouble(0, 59));
            Assert.True("59".IsRangeDouble(0, 59));
            Assert.False("60".IsRangeDouble(0, 59));

            Assert.True("0.1".IsRangeDouble(0.1, 9.9));
            Assert.True("0.2".IsRangeDouble(0.1, 9.9));
            Assert.False("10.2".IsRangeDouble(0.1, 9.9));
        }

    }
}
