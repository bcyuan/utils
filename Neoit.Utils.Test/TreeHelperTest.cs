﻿using Neoit.Utils.Extensions.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Neoit.Utils.Test
{
    public class TreeHelperTest
    {
        #region ToTree
        [Fact]
        public void ToTree()
        {
            var list = GetIParentId_FixedList();
            var tree = list.ToTree<TestTree, int>();
            Assert.NotNull(tree);
            Assert.Equal(2, tree.Count);
            Assert.Equal(5, tree[1].Children.ElementAt(0).Id);

            Assert.Equal(3, tree[0].Children.ElementAt(0).Id);
            Assert.Equal(4, tree[0].Children.ElementAt(1).Id);

            Assert.Equal(6, tree[0].Children.ElementAt(1).Children.ElementAt(0).Id);
        }

        [Fact]
        public void ToTreeError()
        {
            var list = new List<TestTree> {
                new TestTree{Id=0,ParentId=888},
                new TestTree{Id=1,ParentId=3},
                new TestTree{Id=2,ParentId=1},
                new TestTree{Id=3,ParentId=2}
            };
            var exception = Assert.Throws<ArgumentException>(() => list.ToTree<TestTree, int>());
            var tipPrex = "数据源中存在循环引用项";
            Assert.Equal($"{tipPrex}：Id=1", exception.Message);

            list = new List<TestTree> {
                new TestTree{Id=1,ParentId=2},
                new TestTree{Id=2,ParentId=1}
            };
            exception = Assert.Throws<ArgumentException>(() => list.ToTree<TestTree, int>());
            Assert.Equal($"{tipPrex}：Id=1", exception.Message);
        }
        #endregion

        #region Flatten
        [Fact]
        public void Flatten()//Flatten
        {
            var tree = GetIChildren_FixedList();
            var list = tree.Flatten<TestChildren, int>().ToList();

            Assert.Equal(8, list.Count);
            Assert.Equal(1, list[0].Id);
            Assert.Equal("11", list.FirstOrDefault(s => s.Id == 11).Name);
        }
        #endregion

        #region 搜索
        //[Fact]
        //public void FlattenDataFindChildren()
        //{
        //    var list = GetFixedListWithParent();
        //    var datas = TreeHelper.FlattenDataFindChildren(list, 1);
        //    Assert.Equal("3,4,6", datas.Select(s => s.Id).ToList().JoinString());
        //}
        #endregion

        #region data
        private List<TestTree> GetIParentId_FixedList()//固定的测试数据，请勿修改
        {
            return new List<TestTree> {
                new TestTree{Id=1,ParentId=0},
                new TestTree{Id=2,ParentId=0},
                new TestTree{Id=3,ParentId=1},
                new TestTree{Id=4,ParentId=1},
                new TestTree{Id=5,ParentId=2},
                new TestTree{Id=6,ParentId=4}
            };
        }

        private List<TestChildren> GetIChildren_FixedList()//固定的测试数据，请勿修改
        {
            return new List<TestChildren> {
                new TestChildren{Id=1,Children=new List<TestChildren>{
                    new TestChildren{Id=11,Name="11"},
                    new TestChildren{Id=12},
                } },
                new TestChildren{Id=2,Children=new List<TestChildren>{
                    new TestChildren{Id=21,Children=new List<TestChildren>{
                        new TestChildren{Id=211},
                        new TestChildren{Id=212}
                    } },
                    new TestChildren{Id=22},
                } },
            };
        }
        #endregion
    }

    #region ITreeChildren
    public class TestChildren : ITreeChildren<TestChildren, int>
    {
        public string Name { get; set; }
        public IEnumerable<TestChildren> Children { get; set; }
        public int Id { get; set; }
    }
    #endregion

    #region ITreeParentId、ITreeChildren
    public class TestTree : ITreeParentId<TestTree, int>, ITreeChildren<TestTree, int>
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public IEnumerable<TestTree> Children { get; set; }
    }
    #endregion


}
