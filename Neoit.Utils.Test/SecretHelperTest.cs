﻿using Xunit;

namespace Neoit.Utils.Test
{
    public class SecretHelperTest
    {
        [Fact]
        public void PersonalAccessToken()
        {
            Assert.True(SecretHelper.VerifyPersonalAccessToken(SecretHelper.PersonalAccessToken()));
            Assert.True(SecretHelper.VerifyPersonalAccessToken(SecretHelper.PersonalAccessToken("abc"),"abc"));
            Assert.False(SecretHelper.VerifyPersonalAccessToken(SecretHelper.PersonalAccessToken("a"),"b"));

            Assert.False(SecretHelper.VerifyPersonalAccessToken(""));
            Assert.False(SecretHelper.VerifyPersonalAccessToken("1"));
            Assert.False(SecretHelper.VerifyPersonalAccessToken("aaa.bbb.ccc"));
            Assert.False(SecretHelper.VerifyPersonalAccessToken("z26o56qydrnxuaygun3jbcxbs4tvivnxq24dafhapcwxg5lcjkeq"));
        }
    }
}
