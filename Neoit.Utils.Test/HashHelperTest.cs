﻿using System;
using Xunit;

namespace Neoit.Utils.Test
{
    public class HashHelperTest
    {
        [Fact]
        public void MD5_16()
        {
            Assert.Equal("a0b923820dcc509a", HashHelper.MD5_16("1"));
        }
        [Fact]
        public void MD5_32()
        {
            Assert.Equal("c4ca4238a0b923820dcc509a6f75849b", HashHelper.MD5_32("1"));
        }
        [Fact]
        public void Sha1()
        {
            Assert.Equal("356a192b7913b04c54574d18c28d46e6395428ab", HashHelper.Sha1("1"));
            Assert.Equal(40, HashHelper.Sha1(DateTime.Now.GetHashCode().ToString()).Length);
        }
        [Fact]
        public void Sha256()
        {
            Assert.Equal("6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b", HashHelper.Sha256("1"));
            Assert.Equal(64, HashHelper.Sha256(DateTime.Now.GetHashCode().ToString()).Length);
        }
        [Fact]
        public void Sha512()
        {
            Assert.Equal("4dff4ea340f0a823f15d3f4f01ab62eae0e5da579ccb851f8db9dfe84c58b2b37b89903a740e1ee172da793a6e79d560e5f7f9bd058a12a280433ed6fa46510a", HashHelper.Sha512("1"));
            Assert.Equal(128, HashHelper.Sha512(DateTime.Now.GetHashCode().ToString()).Length);
        }

    }
}
