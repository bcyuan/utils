﻿using Neoit.Utils.Extensions;
using System;
using Xunit;

namespace Neoit.Utils.Test
{
    public class OtherExtensionTest
    {
        [Fact]
        public void Combine()
        {
            Assert.Equal("a", new Exception("a").Combine());
            Assert.Equal($"a{Environment.NewLine}b{Environment.NewLine}c", new Exception("a", new Exception("b", new Exception("c"))).Combine());
            Assert.Equal($"a,b,c", new Exception("a", new Exception("b", new Exception("c"))).Combine(","));
        }

    }
}
