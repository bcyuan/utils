﻿using Neoit.Utils.Extensions;
using System;
using Xunit;
using static Neoit.Utils.Extensions.DateTimeExtension;

namespace Neoit.Utils.Test
{
    public class DateTimeExtensionTest
    {
        #region 时间格式化
        [Fact]
        public void Date()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal("2088/08/08", date.Date());
        }
        [Fact]
        public void DateTimeTest()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal("2088/08/08 08:08:08", date.DateTime());
        }
        #endregion

        #region 获取指定时间
        [Fact]
        public void NextDay()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal(Convert.ToDateTime("2088/08/09 00:00:00"), date.NextDay());
        }
        [Fact]
        public void MonthStart()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal(Convert.ToDateTime("2088/08/01 00:00:00"), date.MonthStart());
        }
        [Fact]
        public void NextMonthStart()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal(Convert.ToDateTime("2088/09/01 00:00:00"), date.NextMonthStart());
        }
        [Fact]
        public void MonthEndOfSecond()
        {
            var date = Convert.ToDateTime("2088/08/08 08:08:08");
            Assert.Equal(Convert.ToDateTime("2088/08/31 23:59:59"), date.MonthEndOfSecond());
        }
        #endregion

        [Fact]
        public void IsOverlap()
        {
            //rang1固定不变，变动rang2

            //范围左边-不重合
            var rang11 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang12 = (Convert.ToDateTime("1999/09/01 00:00:00"), Convert.ToDateTime("1999/12/12 23:59:59"));
            Assert.False(rang11.IsOverlap(rang12));
            //范围左边-临界
            var rang21 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang22 = (Convert.ToDateTime("1999/09/01 00:00:00"), Convert.ToDateTime("2000/01/01 00:00:00"));
            Assert.True(rang21.IsOverlap(rang22));
            //范围左边-部分重合
            var rang31 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang32 = (Convert.ToDateTime("1999/09/01 00:00:00"), Convert.ToDateTime("2000/02/01 00:00:00"));
            Assert.True(rang31.IsOverlap(rang32));
            //包围
            var rang41 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang42 = (Convert.ToDateTime("1999/09/01 00:00:00"), Convert.ToDateTime("2002/12/12 00:00:00"));
            Assert.True(rang41.IsOverlap(rang42));
            //范围右边-部分重合
            var rang51 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang52 = (Convert.ToDateTime("2000/02/01 00:00:00"), Convert.ToDateTime("2002/12/12 00:00:00"));
            Assert.True(rang51.IsOverlap(rang52));
            //范围右边-临界
            var rang61 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang62 = (Convert.ToDateTime("2001/12/12 23:59:59"), Convert.ToDateTime("2002/12/12 00:00:00"));
            Assert.True(rang61.IsOverlap(rang62));
            //范围右边-不重合
            var rang71 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
            var rang72 = (Convert.ToDateTime("2002/12/12 23:59:59"), Convert.ToDateTime("2003/12/12 00:00:00"));
            Assert.False(rang71.IsOverlap(rang72));
        }
        [Fact]
        public void UnixTimestampSecond()
        {
            var date = Convert.ToDateTime("1970/01/01 08:00:00");
            Assert.Equal(0, date.UnixTimestampSecond());
        }
        [Fact]
        public void UnixTimestampMillisecond()
        {
            var date = Convert.ToDateTime("1970/01/01 08:00:00");
            Assert.Equal(0, date.UnixTimestampMillisecond());
        }
        [Fact]
        public void TimeElapsedTest()
        {
            var now = DateTime.Now;

            Assert.Equal("", now.AddMinutes(1).TimeElapsed());
            Assert.Equal("刚刚", now.TimeElapsed());

            Assert.Equal("1秒前", now.AddSeconds(-1).TimeElapsed());
            Assert.Equal(DateTime.Now.AddSeconds(-61).ToString(), DateTime.Now.AddSeconds(-61).TimeElapsed());
            Assert.Equal("1分钟前", DateTime.Now.AddSeconds(-61).TimeElapsed(TimeScale.min));

            Assert.Equal(DateTime.Now.AddHours(-1).ToString(), DateTime.Now.AddHours(-1).TimeElapsed(TimeScale.min));
            Assert.Equal("1小时前", DateTime.Now.AddHours(-1).TimeElapsed(TimeScale.hour));
            Assert.Equal("1小时前", DateTime.Now.AddHours(-1).TimeElapsed(TimeScale.day));

            Assert.Equal(DateTime.Now.AddDays(-1).ToString(), DateTime.Now.AddDays(-1).TimeElapsed(TimeScale.hour));
            Assert.Equal("1天前", DateTime.Now.AddDays(-1).TimeElapsed(TimeScale.day));
            Assert.Equal("1天前", DateTime.Now.AddDays(-1).TimeElapsed(TimeScale.month));

            Assert.Equal(DateTime.Now.AddMonths(-1).ToString(), DateTime.Now.AddMonths(-1).TimeElapsed(TimeScale.day));
            Assert.Equal("1月前", DateTime.Now.AddMonths(-1).TimeElapsed(TimeScale.month));

            var endMonth = new DateTime(DateTime.Today.Year, 12, 31);
            var monthValue = DateTime.Now.Month - endMonth.Month;
            var monthCount = monthValue > 0 ? monthValue : monthValue + 12;
            Assert.Equal($"{monthCount}月前", new DateTime(DateTime.Today.Year - 1, 12, 31).TimeElapsed(TimeScale.month));
            Assert.Equal("1月前", DateTime.Now.AddMonths(-1).TimeElapsed(TimeScale.year));

            Assert.Equal(DateTime.Now.AddYears(-1).ToString(), DateTime.Now.AddYears(-1).TimeElapsed(TimeScale.month));
            Assert.Equal("1年前", DateTime.Now.AddYears(-1).TimeElapsed(TimeScale.year));

        }

        [Fact]
        public void TimeHowLong()
        {
            var now = DateTime.Now;

            Assert.Equal("过期", now.AddMinutes(-1).TimeHowLong());
            Assert.Equal("", now.AddMinutes(-1).TimeHowLong(expiredDisplay: ""));
            Assert.Equal("即将", now.TimeHowLong());

            Assert.Equal("1秒后", now.AddSeconds(1).TimeHowLong(nowTime: now));
            Assert.Equal(now.AddSeconds(61).ToString(), now.AddSeconds(61).TimeHowLong());
            Assert.Equal("1分钟后", now.AddSeconds(61).TimeHowLong(TimeScale.min, nowTime: now));

            Assert.Equal(now.AddHours(1).ToString(), now.AddHours(1).TimeHowLong(TimeScale.min, nowTime: now));
            Assert.Equal("1小时后", now.AddHours(1).TimeHowLong(TimeScale.hour, nowTime: now));
            Assert.Equal("1小时后", now.AddHours(1).TimeHowLong(TimeScale.day, nowTime: now));

            Assert.Equal(now.AddDays(1).ToString(), now.AddDays(1).TimeHowLong(TimeScale.hour, nowTime: now));
            Assert.Equal("1天后", now.AddDays(1).TimeHowLong(TimeScale.day, nowTime: now));
            Assert.Equal("1天后", now.AddDays(1).TimeHowLong(TimeScale.month, nowTime: now));

            Assert.Equal(now.AddMonths(1).ToString(), now.AddMonths(1).TimeHowLong(TimeScale.day, nowTime: now));
            Assert.Equal("1月后", now.AddMonths(1).TimeHowLong(TimeScale.month, nowTime: now));

            var endMonth = new DateTime(now.Year, 1, 1);
            var monthValue = endMonth.Month - now.Month;
            var monthCount = monthValue > 0 ? monthValue : monthValue + 12;
            Assert.Equal($"{monthCount}月后", new DateTime(now.Year + 1, 1, 1).TimeHowLong(TimeScale.month, nowTime: now));
            Assert.Equal("1月后", now.AddMonths(1).TimeHowLong(TimeScale.year, nowTime: now));

            Assert.Equal(now.AddYears(1).ToString(), now.AddYears(1).TimeHowLong(TimeScale.month, nowTime: now));
            Assert.Equal("1年后", now.AddYears(1).TimeHowLong(TimeScale.year, nowTime: now));

        }
    }
}
