using Neoit.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xunit;
using Neoit.Utils.Tools;

namespace Neoit.Utils.Test
{
    public class StringExtensionTest
    {
        [Theory]
        [InlineData(" ", true)]
        [InlineData("a", true)]
        [InlineData("", false)]
        [InlineData(null, false)]
        public void Has(string str, bool result)
        {
            if (result)
            {
                Assert.True(str.Has());
            }
            else
            {
                Assert.False(str.Has());
            }

        }
        [Theory]
        [InlineData(true, "123a", @"\d+")]
        [InlineData(true, "123a", "")]
        [InlineData(false, null, @"\d+")]
        [InlineData(false, "123a", null)]
        [InlineData(false, "", @"\d+")]
        public void IsMatchOrContainsRegex(bool result, string str, string pattern)
        {
            if (result)
            {
                Assert.True(str.IsMatch(pattern));
                Assert.True(str.ContainsRegex(pattern));
            }
            else
            {
                Assert.False(str.IsMatch(pattern));
                Assert.False(str.ContainsRegex(pattern));
            }
        }
        [Fact]
        public void IsMatch_Pattern()
        {
            //手机号
            Assert.True("18812345678".IsMatch(RegexPattern.Mobile));
            Assert.False("a18812345678".IsMatch(RegexPattern.Mobile));
            Assert.False("1881a345678".IsMatch(RegexPattern.Mobile));
            Assert.False("1881234567".IsMatch(RegexPattern.Mobile));

            Assert.True("440102198001021230".IsMatch(RegexPattern.IdCard));
            Assert.True("44010219800102123x".IsMatch(RegexPattern.IdCard));
            Assert.True("44010219800102123X".IsMatch(RegexPattern.IdCard));
            Assert.False("44010219800102123".IsMatch(RegexPattern.IdCard));
            Assert.False("44010219800102123A".IsMatch(RegexPattern.IdCard));
        }
        [Fact]
        public void IsMatchIgnoreCase()
        {
            Assert.True("A123b".IsMatchIgnoreCase("a"));
            Assert.True("A123b".ContainsRegexIgnoreCase("a"));
        }
        [Fact]
        public void ContainsRegex()
        {
            Assert.True("a123b".ContainsRegex("a", @"\d+"));
        }
        [Theory]
        [InlineData("123", "123a", @"\d+")]
        [InlineData(null, null, null)]
        [InlineData(null, null, @"\d+")]
        [InlineData("", "aa", @"\d+")]
        public void Match(string result, string str, string pattern)
        {
            Assert.Equal(result, str.Match(pattern));
        }

        [Fact]
        public void Matchs()
        {
            Assert.Equal(new List<string> { "1", "2", "3" }, "1a2b3c".Matchs(@"\d+"));
            Assert.Empty("1a2b3c".Matchs(null));
        }
        [Fact]
        public void MatchGroup()
        {
            Assert.Equal(new List<string> { "1" }, "1a(2)b3c".MatchGroup(@"\d+"));
            Assert.Equal(new List<string> { "41272419880522659X", "412724", "19880522", "659X" }, "41272419880522659X@@@".MatchGroup(@"(\d{6})(\d{8})(\w{4})"));
            Assert.Empty("1a2b3c".MatchGroup(null));
            Assert.Empty("aaa".MatchGroup("\b"));
        }
        [Fact]
        public void MatchGroupFirst()
        {
            Assert.Equal("123", "a123b".MatchGroupFirst(@"a(\d+)b"));
            Assert.Null("1a2b3c".MatchGroupFirst(null));
            Assert.Null("aaa".MatchGroupFirst("\b"));
        }
        [Fact]
        public void ReplaceRegex()
        {
            Assert.Null("1a2b3c".ReplaceRegex(null, null));
            Assert.Equal("abc", "abc".ReplaceRegex(@"\d+", @"88"));
            Assert.Equal("88ab88", "13ab45".ReplaceRegex(@"\d+", @"88"));
            Assert.Equal("88xx88", "88aB88".ReplaceRegex(@"ab", @"xx", RegexOptions.IgnoreCase));
        }
        [Fact]
        public void Between()
        {
            Assert.Equal("123", "a123b".Between("a", "b"));
            Assert.Equal("123b", "a123b".Between("a"));
            Assert.Equal("a123", "a123b".Between(null, "b"));

            Assert.Equal("123", "ba123b".Between("a", "b"));

            Assert.Null("a123b".Between(null, null));
            Assert.Equal("", "a123b".Between("xx", "b"));//若找不到标签，应该期望返回"";即采取保守策略
            Assert.Equal("", "a123b".Between("a", "xx"));
            Assert.Equal("", "a123b".Between("xx", "xx"));

            Assert.Equal("a123b", "1a123b2".Between("a", "b", containsTag: true));
            Assert.Equal("a123b", "xa123b".Between("a", null, containsTag: true));
            Assert.Equal("a123b", "a123bx".Between(null, "b", containsTag: true));
        }
        [Fact]
        public void RegexBetween()
        {
            Assert.Null("1a2b3c".BetweenRegex(null, null));
            Assert.Equal("abc", "123abc456".BetweenRegex(@"\d{3}", @"\d{3}"));
            Assert.Null("".BetweenRegex(@"\d{3}", @"\d{3}"));
            Assert.Null("".BetweenRegex(@"\d{3}", null));
            Assert.Null("123abc456".BetweenRegex(@"a", @"C"));
            Assert.Equal("b", "123abc456".BetweenRegex(@"a", @"C", RegexOptions.IgnoreCase));
            Assert.Equal("b", "123abc456".BetweenRegexIgnoreCase(@"a", @"C"));

        }
        class ToJsonTest { }
        [Fact]
        public void ToJson()
        {
            var listNameValue = new List<dynamic> {
                new { Name = "a", Value = "1" },
                new { Name = "b", Value = "2", DateTime = Convert.ToDateTime("2088/8/8 08:08") } };
            ToJsonTest nameValueNull = null;

            Assert.Equal("{\"nameA\":\"a\",\"value\":\"1\"}", new { NameA = "a", Value = "1" }.ToJson());
            Assert.Equal("[1,2,3]", new List<int> { 1, 2, 3 }.ToJson());
            Assert.Equal("[{\"name\":\"a\",\"value\":\"1\"},{\"name\":\"b\",\"value\":\"2\",\"dateTime\":\"2088/08/08 08:08:00\"}]", listNameValue.ToJson());
            Assert.Equal("", nameValueNull.ToJson());

            Assert.Equal("{\"nameA_A\":\"a\",\"a_b\":\"1\"}", new { NameA_A = "a", a_b = "1" }.ToJson());
        }
        [Fact]
        public void ToJsonWithOriginal()
        {
            Assert.Equal("{\"NameA\":\"a\",\"Value\":\"1\"}", new { NameA = "a", Value = "1" }.ToJsonWithOriginal());
        }
        [Fact]
        public void ToEntity()
        {
            Assert.Equal("{\"name\":\"a\",\"value\":\"1\"}", "{\"name\":\"a\",\"value\":\"1\"}".ToEntity<NameValue>().ToJson());
            Assert.Equal("[1,2,3]", "[1,2,3]".ToEntity<List<int>>().ToJson());

            Assert.Throws<JsonReaderException>(() => "".ToEntity<List<int>>().ToJson());
            Assert.Throws<JsonReaderException>(() => "{\"name\":\"a\"".ToEntity<List<int>>().ToJson());
        }
        [Fact]
        public void JsonValue()
        {
            Assert.Null("".JsonValue("a"));
            Assert.Null("a".JsonValue(""));
            Assert.Null("[1,2,3]".JsonValue("[3]"));
            Assert.Null("[1,2,3]".JsonValue("name"));

            Assert.Equal("a", "{\"name\":\"a\",\"value\":\"1\"}".JsonValue("name"));//普通对象
            Assert.Equal("666", "{\"name\":\"a\",\"value\":{\"name\":\"666\",\"value\":\"1\"}}".JsonValue("value.name"));//嵌套对象
            Assert.Equal("1", "[1,2,3]".JsonValue("[0]"));//普通数组
            Assert.Equal("[", "[\"a\",\"[\",\"c\"]".JsonValue("[1]"));//不支持特殊字符
            Assert.Equal("3", "[1,2,[1,2,3]]".JsonValue("[2][2]"));//嵌套数组

            Assert.Equal("a", "[{\"name\":\"a\",\"value\":\"1\"},{\"name\":\"b\",\"value\":\"2\",\"dateTime\":\"2088/08/08 08:08:00\"}]".JsonValue("[0].name"));//数组中对象
            Assert.Equal("1", "{\"name\":\"a\",\"value\":[1,2,3]}".JsonValue("value[0]"));//对象中数组

            Assert.Throws<JsonReaderException>(() => "a".JsonValue("a"));
        }
        [Fact]
        public void Dynamic()
        {
            Assert.Equal("a", "{\"name\":\"a\",\"value\":\"1\"}".Dynamic().name.Value);
            Assert.Null("".Dynamic());

            Assert.Throws<JsonReaderException>(() => "a".Dynamic());
            Assert.Throws<JsonSerializationException>(() => "{\"name\":\"[a\"".Dynamic());
        }

        [Fact]
        public void FirstToUpper()
        {
            string nullv = null;
            Assert.Null(nullv.FirstToLower());
            Assert.Equal("Abc", "abc".FirstToUpper());
            Assert.Equal("A dog", "a dog".FirstToUpper());
            Assert.Equal("12a", "12a".FirstToUpper());
        }
        [Fact]
        public void FirstToLower()
        {
            string nullv = null;
            Assert.Null(nullv.FirstToLower());
            Assert.Equal("abc", "Abc".FirstToLower());
            Assert.Equal("a dog", "A dog".FirstToLower());
            Assert.Equal("12a", "12a".FirstToLower());
        }
        [Fact]
        public void FormatDate()
        {
            string nullV = null;
            Assert.Null(nullV.FormatDate());
            Assert.Equal("", "".FormatDate());
            Assert.Equal("20880808080800", "2088/08/08 08:08".FormatDate("yyyyMMddHHmmss"));

            Assert.Equal("2088/08/08", "2088/8/8 08:08".FormatDate());
            Assert.Equal("2088/08/08", "2088-8-8 08:08".FormatDate());
            Assert.Equal("2088/08/08 08:08:00", "2088-8-8 08:08".FormatDateTime());
        }

        [Fact]
        public void TimestampToTime()
        {
            Assert.Null("".TimestampToTime());
            Assert.Equal(Convert.ToDateTime("1970/01/01T00:00:00"), "0".TimestampToTime());//2088/03/04 05:06:07
            Assert.Equal(Convert.ToDateTime("1970-01-01T00:00:00"), "0".TimestampSecondsToTime());//2088/03/04 05:06:07
        }

        [Fact]
        public void Omit()
        {
            Assert.Equal("北京多年...", "北京多年来坚持实行".Omit(4));
            Assert.Equal("北京多年......", "北京多年来坚持实行".Omit(4, "......"));
            Assert.Equal("北京", "北京".Omit(4));
        }
        [Fact]
        public void MaxLength()
        {
            Assert.Equal("北京多年", "北京多年来坚持实行".MaxLength(4, null));
            Assert.Equal("北...", "北京多年来坚持实行".MaxLength(4));
            Assert.Equal("北京多年", "北京多年来坚持实行".MaxLength(4, "......"));
            Assert.Equal("北京", "北京".MaxLength(4));
            Assert.Equal("北京多年", "北京多年".MaxLength(4, "****"));
            Assert.Equal("北京多年", "北京多年来".MaxLength(4, "****"));
            Assert.Equal("北京多年来", "北京多年来".MaxLength(5, "****"));
            Assert.Equal("北****", "北京多年来坚持".MaxLength(5, "****"));
            Assert.Equal("北", "北".MaxLength(5, "****"));
        }
        [Fact]
        public void Hidden()
        {
            Assert.Equal("188****8888", "18888888888".Hidden(3, 4));
            Assert.Equal("188**8888", "18888888888".Hidden(3, 4, "**"));
            Assert.Equal("18888888888", "18888888888".Hidden(6, 6));
        }

        [Fact]
        public void SplitReserveSeparator()
        {
            Assert.Equal(new List<string> { "0", ",", "1" }, "0,1".SplitReserveSeparator(","));
            Assert.Equal(new List<string> { "12" }, "12".SplitReserveSeparator("12"));
            Assert.Equal(new List<string> { "12" }, "12".SplitReserveSeparator("ab"));
            Assert.Equal(new List<string> { "12" }, "12".SplitReserveSeparator(""));
        }

        [Fact]
        public void ReplaceSeparatorAsComma()
        {
            Assert.Equal("a,b,c,d,e,f,g,h", "a,b，c;d；e|f$g、h".ReplaceSeparator());
        }

        [Fact]
        public void ParseCronBySecond()
        {
            Assert.Equal("每年1月1日1:00", "0 0 1 1 1 ?".ParseCronBySecond());
            Assert.Equal("每年1月、6月的1日1:00", "0 0 1 1 1,6 ?".ParseCronBySecond());
            Assert.Equal("每月1日1:00", "0 0 1 1 * ?".ParseCronBySecond());
            Assert.Equal("每月15日10:15", "0 15 10 15 * ?".ParseCronBySecond());
            Assert.Equal("每月最后一天10:15", "0 15 10 L * ?".ParseCronBySecond());
            Assert.Equal("每月倒数第2天10:15", "0 15 10 L-2 * ?".ParseCronBySecond());

            Assert.Equal("每月最后一个周五10:15", "0 15 10 ? * 6L".ParseCronBySecond());
            Assert.Equal("每月第3个周五10:15", "0 15 10 ? * 6#3".ParseCronBySecond());

            Assert.Equal("每天12:00", "0 0 12 * * ?".ParseCronBySecond());
            Assert.Equal("每天10:15", "0 15 10 * * ?".ParseCronBySecond());

            Assert.Equal("每30秒", "0/30 * * * * ?".ParseCronBySecond());
            Assert.Equal("每秒", "* * * * * ?".ParseCronBySecond());
            Assert.Equal("每分钟", "0 0/1 * * * ?".ParseCronBySecond());
            Assert.Equal("每小时", "0 0 0/1 * * ?".ParseCronBySecond());

            Assert.Equal("每天1:00", "0 0 1 * * ?".ParseCronBySecond());

            Assert.Equal("2050年里的每天10:15", "0 15 10 * * ? 2050".ParseCronBySecond());

            Assert.Equal("每天14时里的每分钟", "0 * 14 * * ?".ParseCronBySecond());
            Assert.Equal("每天14时、18时里的每5分钟", "0 0/5 14,18 * * ?".ParseCronBySecond());

            Assert.Equal("每天14时，0-5分里的每分钟", "0 0-5 14 * * ?".ParseCronBySecond());

            Assert.Equal("每年11月11日11:11", "0 11 11 11 11 ?".ParseCronBySecond());

            Assert.Equal("每年3月里的每周三14时10分、44分", "0 10,44 14 ? 3 WED".ParseCronBySecond());//周
            Assert.Equal("周一至周五里的每天10:15", "0 15 10 * ? mon-fri".ParseCronBySecond());//周
            Assert.Equal("每月，周六至周日里的每天1:00", "0 0 1 ? * 6-7".ParseCronBySecond());//周
            Assert.Equal("每周日1:00", "0 0 1 ? * SUN".ParseCronBySecond());//周
            Assert.Equal("每年2月，周一至周三里的每天1:00", "0 0 1 ? 2 MON-WED".ParseCronBySecond());//周

            Assert.Equal("2002-2010年的每年1月、3月、9月的，周一至周五的每小时，14,18,3-39,52分里的每5秒", "0/5 14,18,3-39,52 * ? JAN,MAR,SEP MON-FRI 2002-2010".ParseCronBySecond());

            Assert.Equal("每年1月1日1时1分、2分、3分", "0 1,2,3 1 1 1 ?".ParseCronBySecond());
            Assert.Equal("每年1月离10日最近的工作日1:00", "0 0 1 10W 1 ?".ParseCronBySecond());


            Assert.Throws<Exception>(() => "".ParseCronBySecond());
            Assert.Throws<Exception>(() => "xxxxx".ParseCronBySecond());
            Assert.Throws<Exception>(() => "0 0 22 ? 1 ?".ParseCronBySecond());

            Assert.Equal("每年1月1日25:00", "0 0 25 1 1 ?".ParseCronBySecond(isValid:false));

        }

        [Fact]
        public void ParseCronByMinute()
        {
            Assert.Equal("每年1月1日1:00", "0 1 1 1 ?".ParseCronByMinute());
            Assert.Equal("每年1月、6月的1日1:00", "0 1 1 1,6 ?".ParseCronByMinute());

        }

        [Fact]
        public void Similarity()
        {
            Assert.Equal(1, "hello".SimilarityCosine("hello"));
            Assert.Equal(1, "开具证明".SimilarityCosine("证明开具"));
            Assert.Equal(0.82, "我要开具证明".SimilarityCosine("开具证明"));
            Assert.Equal(0.75, "hello".SimilarityCosine("hella"));

            Assert.Equal(0, "a".SimilarityCosine("b"));
            Assert.Equal(0, "a".SimilarityCosine(null));
            Assert.Equal(0, Global.nullStr.SimilarityCosine("abc"));
        }

        [Fact]
        public void Trim()
        {
            Assert.Equal("123", "abc123abc".Trim("abc"));
            Assert.Equal("123abc", "abc123abc".TrimStart("abc"));
            Assert.Equal("abc123", "abc123abc".TrimEnd("abc"));
            Assert.Equal("abc123", "abc123".TrimEnd("abc"));

            Assert.Equal("abc123abc", "abc123abc".Trim("xx"));
            Assert.Equal("abc123abc", "abc123abc".TrimStart("xx"));
            Assert.Equal("abc123abc", "abc123abc".TrimEnd("xx"));
        }
    }
}
