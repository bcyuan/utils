﻿using Neoit.Utils.Extensions;
using System.Collections.Generic;
using System.Net.Http;
using Xunit;

namespace Neoit.Utils.Test
{
    public class HttpHelperTest
    {
        [Fact]
        public void ConvertToHttpMethod()
        {
            Assert.Equal(HttpMethod.Get, HttpHelper.ConvertToHttpMethod("Get"));
            Assert.Equal(HttpMethod.Post, HttpHelper.ConvertToHttpMethod("Post"));
            Assert.Equal(HttpMethod.Delete, HttpHelper.ConvertToHttpMethod("Delete"));
            Assert.Equal(HttpMethod.Put, HttpHelper.ConvertToHttpMethod("Put"));
            Assert.Equal(HttpMethod.Patch, HttpHelper.ConvertToHttpMethod("Patch"));
            Assert.Equal(HttpMethod.Options, HttpHelper.ConvertToHttpMethod("Options"));
            Assert.Equal(HttpMethod.Trace, HttpHelper.ConvertToHttpMethod("Trace"));
            Assert.Equal(HttpMethod.Head, HttpHelper.ConvertToHttpMethod("Head"));

            Assert.Equal(HttpMethod.Get, HttpHelper.ConvertToHttpMethod("geT"));
            Assert.Equal(HttpMethod.Get, HttpHelper.ConvertToHttpMethod("GET"));

            Assert.Null(HttpHelper.ConvertToHttpMethod("xxx"));
        }
        [Fact]
        public void ConvertToHeaderDic()
        {
            Assert.Equal(new Dictionary<string, string> { { "key1", "value1" } }.ToJson(), HttpHelper.ConvertToHeaderDic("key1: value1").ToJson());

            string headers = @"key1: value1
key2: value2
";
            Assert.Equal(new Dictionary<string, string> { { "key1", "value1" }, { "key2", "value2" } }.ToJson(), HttpHelper.ConvertToHeaderDic(headers).ToJson());


            string headersAll = @"cookie: koa:sess=eyJ1c2VySWQiOjQyMTM2MywiX2V4cGlNDYwiX21heEFnZSI6MjU5MjAwMDAwMDB9; koa:sess.sig=JBDXtPwA4cqvOiF_RYL5RXkHe14; _gid=GA1.2.84607819.1709098765; _ga=GA1.1.585007095.1708913937; _ga_CZFVKMNT9J=GS1.1.1709098765.3.1.1709098789.0.0.0
 :authority: glados.space
:path: /api/user/checkin
accept-encoding: gzip, deflate, br
";
            var dicAll = new Dictionary<string, string> {
                 {"cookie","koa:sess=eyJ1c2VySWQiOjQyMTM2MywiX2V4cGlNDYwiX21heEFnZSI6MjU5MjAwMDAwMDB9; koa:sess.sig=JBDXtPwA4cqvOiF_RYL5RXkHe14; _gid=GA1.2.84607819.1709098765; _ga=GA1.1.585007095.1708913937; _ga_CZFVKMNT9J=GS1.1.1709098765.3.1.1709098789.0.0.0" },
                 {":authority","glados.space" },
                 {":path","/api/user/checkin" },
                 {"accept-encoding","gzip, deflate, br" },
             };
            Assert.Equal(dicAll.ToJson(), HttpHelper.ConvertToHeaderDic(headersAll).ToJson());

            string headerBreakLine = "key1: value1\rkey2: value2\nkey3: value3\r\nkey4: value4";
            var dicBreakLine = new Dictionary<string, string> {
                 {"key1","value1" },
                 {"key2","value2" },
                 {"key3","value3" },
                 {"key4","value4" }
             };
            Assert.Equal(dicBreakLine.ToJson(), HttpHelper.ConvertToHeaderDic(headerBreakLine).ToJson());

            Assert.Equal("{}", HttpHelper.ConvertToHeaderDic("xxx").ToJson());
        }

    }
}
