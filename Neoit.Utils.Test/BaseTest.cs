﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using Neoit.Utils.Extensions;
using System;
using Xunit;

namespace Neoit.Utils.Test
{
    public class BaseTest
    {
        [Fact]
        public void CheckForConsoleWriteLineCalls()
        {
            // 读取程序集
            AssemblyDefinition assembly = AssemblyDefinition.ReadAssembly(typeof(StringExtension).Assembly.Location);
            // 列出所有类型
            foreach (TypeDefinition type in assembly.MainModule.Types)
            {
                // 列出所有方法
                foreach (MethodDefinition method in type.Methods)
                {
                    if (method.HasBody)
                    {
                        var body = method.Body;
                        foreach (Instruction instruction in body.Instructions)
                        {
                            if (instruction.OpCode == OpCodes.Call)
                            {
                                MethodReference calledMethod = (MethodReference)instruction.Operand;
                                if (calledMethod.Name == "WriteLine" && calledMethod.DeclaringType.FullName == "System.Console")
                                {
                                    throw new Exception($"{type.Name}.{method.Name}(): Method contains a Console.WriteLine call");
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}
