﻿using Neoit.Utils.Extensions;
using System;
using Xunit;

namespace Neoit.Utils.Test
{
    public class MathExtensionTest
    {
        [Fact]
        public void Round()
        {
            Assert.Equal(3.33m, 3.333m.Round());
            Assert.Equal(3.33m, 3.334m.Round());
            Assert.Equal(3.34m, 3.335m.Round());
            Assert.Equal(3.335m, 3.335m.Round(3));
            Assert.Equal(3.3m, 3.3m.Round(2));

            Assert.Equal(3.3m, Math.Round(3.34m,1));//4舍
            Assert.Equal(3.4m, Math.Round(3.36m, 1));//6入
            Assert.Equal(3.4m, Math.Round(3.35m, 1));//5取偶 case1
            Assert.Equal(3.4m, Math.Round(3.45m, 1));//5取偶 case2
            Assert.Equal(3.5m, 3.45m.Round(1));//对比常规四舍五入
            
        }
        [Fact]
        public void Fix()
        {
            #region decimal
            Assert.Equal("3.33", 3.333m.Fix());
            Assert.Equal("3.33", 3.334m.Fix());
            Assert.Equal("3.34", 3.335m.Fix());

            Assert.Equal("3.30", 3.3m.Fix());
            Assert.Equal("3.00", 3m.Fix());

            Assert.Equal("3.000", 3m.Fix(3));
            Assert.Equal("3.4", 3.35m.Fix(1));
            #endregion

            #region string
            Assert.Equal("3.33", "3.333".Fix());
            Assert.Equal("3.33", "3.334".Fix());
            Assert.Equal("3.34", "3.335".Fix());

            Assert.Equal("3.30", "3.3".Fix());
            Assert.Equal("3.00", "3".Fix());

            Assert.Equal("3.000", "3".Fix(3));
            Assert.Equal("3.4", "3.35".Fix(1));

            Assert.Equal("3.4", "3.35".Fix(1));
            #endregion
        }


    }
}
