﻿using Neoit.Utils.Security;
using System.Security.Cryptography;
using Xunit;

namespace Neoit.Utils.Test
{
    public class RsaHelperTest
    {
        [Fact]
        public void VerifySign()
        {
            var publicKey = @"-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbn01CePHVC+fSxKCF6o2fCeka
pjUtE1MxMablXw1wPAkcIi9jHCUe7TYYlYA75MYnB5grVbia91MLXNi07zrKRKsD
kaxosOMNNg683IPBKUbOf/posa0Bvm0/hIQRbNNLm1YFvIqRCccnyg1Rwb7UsSmU
xQbQQnRmBgDXcHBaCwIDAQAB
-----END PUBLIC KEY-----";
            var sign = "PQkg4lxU0F91ZWISnkxGwpyF+rhno5o4/u3G5f7uXVSXOaKvbf37lXAtnDtOyqV3NHnKpJNbWkgqYii+NkHwNA+ccjRbeQQc2G4EIqtNgjrR/l1ZW/w7s2KSxjaL6kM9AiY1e4OilE2SXT6duYOz1urhErqbv2eUv9b8PB9WcHQ=";
            var data = "1";
            Assert.True(RsaHelper.VerifySign(publicKey, data, sign, HashAlgorithmName.SHA256));

            var publicKey2 = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbn01CePHVC+fSxKCF6o2fCeka
pjUtE1MxMablXw1wPAkcIi9jHCUe7TYYlYA75MYnB5grVbia91MLXNi07zrKRKsD
kaxosOMNNg683IPBKUbOf/posa0Bvm0/hIQRbNNLm1YFvIqRCccnyg1Rwb7UsSmU
xQbQQnRmBgDXcHBaCwIDAQAB";

            Assert.True(RsaHelper.VerifySign(publicKey2, data, sign, HashAlgorithmName.SHA256));
            Assert.False(RsaHelper.VerifySign(publicKey + "xx", data, sign, HashAlgorithmName.SHA256));
            Assert.False(RsaHelper.VerifySign(publicKey, data, "1", HashAlgorithmName.SHA512));
            Assert.False(RsaHelper.VerifySign(publicKey, data, sign, HashAlgorithmName.SHA512));

        }
        [Fact]
        public void CreateTest()
        {
            var key = RsaHelper.CreateRSAKeyPEM();
            Assert.True(key.publicKey.Length > 200);
            Assert.True(key.privateKey.Length > 800);
        }

    }
}
