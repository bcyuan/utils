﻿using Neoit.Utils.Office;
using System;
using System.Collections.Generic;
using Xunit;

namespace Neoit.Utils.Test
{
    public class TableHelperTest
    {
        [Fact]
        public void ConvertToEntity()
        {
            //带标题,且设置起始行索引
            var sourceData = new List<List<string>> {
            new List<string>{ "name","age" },
            new List<string>{ "neoc","18" },
            new List<string>{ "jack","19" },
            };
            var columnInfos = new List<MapperInfo> {
            new MapperInfo(nameof(NameAge.Name),"name"),
            new MapperInfo(nameof(NameAge.Age),"age"),
            };
            var list = TableHelper.ConvertToEntity<NameAge>(sourceData, columnInfos);
            Assert.Equal("neoc", list[0].Name);
            Assert.Equal(18, list[0].Age);

            //索引-顺序：无标题则按columnInfo顺序
            var sourceData2 = new List<List<string>> {
            new List<string>{ "neoc","18" },
            new List<string>{ "jack","19" },
            };
            var columnInfosNoTitle = new List<MapperInfo> {
            new MapperInfo(nameof(NameAge.Note)),
            new MapperInfo(nameof(NameAge.Name)),
            };
            var list2 = TableHelper.ConvertToEntity<NameAge>(sourceData2, columnInfosNoTitle);
            Assert.Equal("neoc", list2[0].Note);
            Assert.Equal("18", list2[0].Name);

            //索引-指定
            var sourceData31 = new List<List<string>> {
            new List<string>{ "name","age" ,"note" },
            new List<string>{ "neoc","18","note" }
            };
            var columnInfosNoTitle31 = new List<MapperInfo> {
            new MapperInfo(nameof(NameAge.Name),0),
            new MapperInfo(nameof(NameAge.Age),"age"),
            new MapperInfo(nameof(NameAge.Note),2)
            };
            var list31 = TableHelper.ConvertToEntity<NameAge>(sourceData31, columnInfosNoTitle31);
            Assert.Equal("neoc", list31[0].Name);
            Assert.Equal(18, list31[0].Age);
            Assert.Equal("note", list31[0].Note);

            //MatchType
            var sourceData3 = new List<List<string>> {
            new List<string>{ "name","age2" ,"note333" ,"mark" },
            new List<string>{ "neoc","18","note","mark" }
            };
            var columnInfosNoTitle3 = new List<MapperInfo> {
            new MapperInfo(nameof(NameAge.Name),"na",MatchType.Contains),
            new MapperInfo(nameof(NameAge.Age),"age2",MatchType.Equal),
            new MapperInfo(nameof(NameAge.Note),@"no\w+",MatchType.Regex),
            new MapperInfo(nameof(NameAge.Note2),@"note2",MatchType.Regex),
            new MapperInfo(nameof(NameAge.Mark),@"MAR",MatchType.RegexIgnoreCase)
            };
            var list3 = TableHelper.ConvertToEntity<NameAge>(sourceData3, columnInfosNoTitle3);
            Assert.Equal("neoc", list3[0].Name);
            Assert.Equal(18, list3[0].Age);
            Assert.Equal("note", list3[0].Note);
            Assert.Null(list3[0].Note2);
            Assert.Equal("mark", list3[0].Mark);

            //ColumnInfo Dic
            var sourceData4 = new List<List<string>> {
            new List<string>{ "name","age2" ,"note333" },
            new List<string>{ "neoc","18","note" }
            };
            var dics = new Dictionary<string, string> {
                { nameof(NameAge.Name),"name"},
                { nameof(NameAge.Age),"age2"},
                { nameof(NameAge.Note),"note333"}
            };
            var list4 = TableHelper.ConvertToEntity<NameAge>(sourceData4, dics);
            Assert.Equal("neoc", list4[0].Name);
            Assert.Equal(18, list4[0].Age);
            Assert.Equal("note", list4[0].Note);

            //ColumnInfo Dic
            var sourceData5 = new List<List<string>> {
            new List<string>{ "name","age2" ,"note333" },
            new List<string>{ "neoc","18","note" }
            };
            var dics5 = new Dictionary<string, string> {
                { nameof(NameAge.Name),"name"},
                { nameof(NameAge.Age),"age"},
                { nameof(NameAge.Note),"note"}
            };
            var list5 = TableHelper.ConvertToEntityLike<NameAge>(sourceData5, dics5);
            Assert.Equal("neoc", list5[0].Name);
            Assert.Equal(18, list5[0].Age);
            Assert.Equal("note", list5[0].Note);

            //ColumnInfo Dic
            var sourceData6 = new List<List<string>> {
            new List<string>{ "name","age2" ,"note333" },
            new List<string>{ "neoc","18" }
            };
            var dics6 = new Dictionary<string, string> {
                { nameof(NameAge.Name),"name"},
                { nameof(NameAge.Age),"age2"}
            };

            Assert.Throws<ArgumentException>(() => TableHelper.ConvertToEntityLike<NameAge>(sourceData6, dics6));

        }

        [Fact]
        public void FindIndexs()
        {
            var sourceData = new List<List<string>> {
            new List<string>{ "name00", "age01" },
            new List<string>{ "name10","age11" },
            new List<string>{ "name20","age21" },
            new List<string>{ "jack","18" },
            };
            var (rowIndex, colIndex) = TableHelper.FindIndexs(sourceData, "age21");
            Assert.Equal(2, rowIndex);
            Assert.Equal(1, colIndex);
        }

    }

    public class NameAge
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Note { get; set; }
        public string Note2 { get; set; }
        public string Mark { get; set; }
    }
}
