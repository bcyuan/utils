﻿using System;
using System.Collections.Generic;
using Xunit;

namespace Neoit.Utils.Test
{
    public class DataTableHelperTest
    {
        [Fact]
        public void ToDataTable()
        {
            var list = new List<NameAge> {
            new NameAge{Name="zs",Age=18 },
            new NameAge{Name="zs2",Age=12 },
            new NameAge{Name="zs3",Age=13 },
            };
            var dt = list.ToDataTable();
            Assert.NotNull(dt);
            Assert.Equal("zs", dt.Rows[0]["Name"]);
            Assert.Equal(18, dt.Rows[0]["Age"]);
        }
        [Fact]
        public void ToList()
        {
            var list = new List<NameAge> {
            new NameAge{Name="zs",Age=18 },
            new NameAge{Name="zs2",Age=12 },
            new NameAge{Name="zs3",Age=13 },
            };
            var dt = list.ToDataTable();
            var listRes = dt.ToList<NameAge>();
            Assert.NotEmpty(listRes);
            Assert.Equal(18, listRes[0].Age);
            Assert.Null(listRes[0].Note);

        }
    }
}
