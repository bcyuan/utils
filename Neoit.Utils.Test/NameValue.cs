﻿namespace Neoit.Utils.Test
{
    public class NameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class NameValue<T>
    {
        public string Name { get; set; }
        public T Value { get; set; }
    }
    public class NameValueNotWrite
    {
        public string Name { get; set; }
        public string Value => "b ";
    }

    public static class NameValueStatic
    {
        public static string Name { get; set; }
        public static int Value { get; set; }
    }
}
