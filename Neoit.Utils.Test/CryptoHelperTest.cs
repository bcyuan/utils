﻿using System;
using System.Text;
using Xunit;

namespace Neoit.Utils.Test
{
    public class CryptoHelperTest
    {
        [Fact]
        public void AES_EncryptBase()
        {
            var key = "815ea00fe6ede0d4c3d1078bc0f24c1c";

            var keyByte = Encoding.UTF8.GetBytes(key);
            var IvByte = Encoding.UTF8.GetBytes(key[..16]);

            var encryptedBytes = CryptoHelper.AES_EncryptBase("1", keyByte, IvByte);
            var encrypted = Convert.ToBase64String(encryptedBytes);
            Assert.Equal("vHfBNucTvsUDlg/HFh3d1Q==", encrypted);

            var decrypt = CryptoHelper.AES_DecryptBase(encryptedBytes, keyByte, IvByte);

            Assert.Equal("1", decrypt);

        }

        [Fact]
        public void AES_Decrypt()
        {
            Assert.Equal("1", CryptoHelper.AES_Decrypt(CryptoHelper.AES_Encrypt("1", "815ea00fe6ede0d4c3d1078bc0f24c1c"), "815ea00fe6ede0d4c3d1078bc0f24c1c"));
            Assert.Equal("vHfBNucTvsUDlg/HFh3d1Q==", CryptoHelper.AES_Encrypt("1", "815ea00fe6ede0d4c3d1078bc0f24c1c"));//验证网址：https://tool.lmeee.com/jiami/aes

        }
    }
}
