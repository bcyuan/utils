﻿using Neoit.Utils.Office;
using Neoit.Utils.ThirdParty.NPOI;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Neoit.Utils.Test
{
    public class NPOIExcelHelperTest
    {
        [Fact]
        public void CreateToByte()
        {
            var datas = new List<Stu>();
            datas.Add(new Stu { Name = "jack", Age = 18, Age2 = "18" });
            datas.Add(new Stu { Name = "zed", Age = 19 });

            var columnInfos = new List<MapperInfo>
            {
                new MapperInfo(nameof(Stu.Age2),"年龄2"),
                new MapperInfo(nameof(Stu.Name),"姓名"),
                new MapperInfo(nameof(Stu.Age),"年龄"),
            };
            byte[] res = ExcelHelper.CreateToByte(datas, columnInfos, new ExcelWriteOption { SheetName = "xxx", HeaderType = ExcelHeaderType.MapperInfoTitle });

            File.WriteAllBytes($"CreateToByte_{DateTime.Now.ToString("HHmmsss")}.xlsx", res);
        }
        [Fact]
        public void CreateToByte_dynamic()
        {
            var datas = new List<dynamic>();
            datas.Add(new { Name = "jack", Age = 18 });
            datas.Add(new { Name = "zed", Age = 19 });

            byte[] res = ExcelHelper.CreateToByte(datas);

            File.WriteAllBytes($"CreateToByte_{DateTime.Now.ToString("HHmmsss")}.xlsx", res);
        }
        [Fact]
        public void CreateToByte_dynamicPropertyName()
        {
            var datas = new List<dynamic>();
            datas.Add(new { Name = "jack", Age = 18 });
            datas.Add(new { Name = "zed", Age = 19 });

            byte[] res = ExcelHelper.CreateToByte(datas, option: new ExcelWriteOption { SheetName = "CreateToByteNoMapperInfo", HeaderType = ExcelHeaderType.dynamicPropertyName });

            File.WriteAllBytes($"CreateToByte_{DateTime.Now.ToString("HHmmsss")}.xlsx", res);
        }
    }

    public class NameAge2
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Note { get; set; }
        public string Note2 { get; set; }
        public string Mark { get; set; }
    }
}
