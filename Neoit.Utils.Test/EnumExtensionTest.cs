﻿using Neoit.Utils.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace Neoit.Utils.Test
{
    public class EnumExtensionTest
    {
        [Fact]
        public void Infos()
        {
            var infos = EnumExtension.Infos<TestEnum>();
            Assert.Equal(3, infos.Count());
            var one = infos.ElementAt(1);
            Assert.Equal(999, one.Value);
            Assert.Equal("Disable", one.Name);
            Assert.Equal(TestEnum.Disable, one.Key);
            Assert.Equal("停用", one.DisplayName);
        }
        [Fact]
        public void GetKeyByName()
        {
            Assert.Equal(999, EnumExtension.GetKeyByName<TestEnum>("Disable"));
            Assert.Equal(999, EnumExtension.GetKeyByName<TestEnum>("disable"));
            Assert.Null(EnumExtension.GetKeyByName<TestEnum>("xxx"));
            Assert.Null(EnumExtension.GetKeyByName<TestEnum>("disable", false));
        }
        [Fact]
        public void GetValueByName()
        {
            Assert.Equal(TestEnum.Disable, EnumExtension.GetValueByName<TestEnum>("Disable"));
            Assert.Equal(TestEnum.Disable, EnumExtension.GetValueByName<TestEnum>("disable"));
            Assert.Null(EnumExtension.GetValueByName<TestEnum>("xxx"));
            Assert.Null(EnumExtension.GetValueByName<TestEnum>("disable", false));
        }

        [Fact]
        public void GetName()
        {
            Assert.Equal("Disable", TestEnum.Disable.GetName());
        }

        [Fact]
        public void GetDisplayName()
        {
            Assert.Equal("停用", TestEnum.Disable.GetDisplayName());
            Assert.Equal("删除", TestEnum.Delete.GetDisplayName());
        }
    }

    public enum TestEnum
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Display(Name = "启用")]
        Enable = 0,
        /// <summary>
        /// 停用
        /// </summary>
        [Display(Name = "停用")]
        Disable = 999,
        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delete = 1000
    }
}
