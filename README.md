# Utils

#### 介绍
C# 常用工具类，精简实用，UT充足
``` csharp
dotnet add package Neoit.Utils
```
#### 功能

1.  EntityExtension

``` csharp
1. ForeachString() //对实体各string属性执行Trim()
    list.ForeachString(s => s == null ? s : s.Trim());
2. entity.ToDic() //实体转字典 
    var entity = new NameValue { Name = "jack", Value = 1 }
    >> {{"Name","jack"},{"Value","1}}
3. DeepCopy() //深拷贝:JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(t))
    entity.DeepCopy();
```

2.  EnumExtension

``` csharp
//示例枚举：
    public enum TestEnum
    {
        [Display(Name = "启用")]
        Enable = 999
    }
1. EnumExtension.Infos<T>() //获取枚举信息集合
    var infos = EnumExtension.Infos<TestEnum>();
    var enumInfo = infos.ElementAt(1);
    enumInfo.Key = 999;
    enumInfo.Value = TestEnum.Enable;
    enumInfo.Name = "Enable";
    enumInfo.DisplayName = "启用";
2. GetKeyByName() //根据name，返回对应int?值
    Assert.Equal(999, EnumExtension.GetKeyByName<TestEnum>("Enable"));
3. GetValueByName() //根据name，返回对应value值
    Assert.Equal(TestEnum.Enable, EnumExtension.GetValueByName<TestEnum>("Enable"));
4. GetName() //根据枚举值获取Name
    Assert.Equal("Enable", TestEnum.Enable.GetName());
5. GetDisplayName() //根据枚举值获取DisplayName
    Assert.Equal("启用", TestEnum.Enable.GetDisplayName());
```
3.  ListExtension

``` csharp
1. Has() //List是否至少有一个元素
    bool Has<T>(this IEnumerable<T> list)
2. EmptyIfNull() //集合若为null，设为Empty
    IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> t)
3. WhereIf() //根据条件执行where方法
    IEnumerable<T> WhereIf<T>(this IEnumerable<T> list, bool condition, Func<T, bool> predicate)
4. Page() //分页
    IEnumerable<T> Page<T>(this IEnumerable<T> list, int pageIndex, int pageSize)
5. RepeatAdjacent() //复制相邻项
    IEnumerable<T> RepeatAdjacent<T>(this IEnumerable<T> t, int repeatCount = 1)
    {1,2,3}.RepeatList(1)  > {1,1,2,2,3,3}
6. GroupByLength() //指定长度分组
    {1,2,3,4,5,6}.GroupByLength(2) >>  [{1,2}{3,4}{5,6}]
7. JoinString() //集合拼接字符串
    {1,2,3}.JoinString() >> "1,2,3"
8. ReduceD() //降维
    IEnumerable<T> ReduceD<T>(this IEnumerable<IEnumerable<T>> lists)
9. DistinctX() //根据属性去重
    IEnumerable<TSource> DistinctX<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
10. Pivot() //Pivot
11. SortByOrder() //指定数组排序
    source.SortByOrder(s => s.Name, new string[] { "petter", "kitty", "jack" });
```
4.  MathExtension

``` csharp
1. Round() //常规保留小数方法(非银行家算法)
    //Math.Round(value, digits, MidpointRounding.AwayFromZero);
    Assert.Equal(3.5m, 3.45m.Round(1))
2. Fix() //固定位数
    Assert.Equal("3.000", 3m.Fix(3));
```
5.  StringExtension

``` csharp
1. Has() //string是否有有效值，即!string.IsNullOrEmpty(str)
    Assert.True("a".Has());
2. IsMatch() //正则：是否匹配
    //依据 Regex.IsMatch(str, pattern)
3. IsMatchIgnoreCase() //正则：是否匹配-忽略大小写
    //依据 Regex.IsMatch(str, pattern, RegexOptions.IgnoreCase);
4. ContainsRegex(params string[] patterns) //正则：是否每个都匹配
    //依据 patterns.All(s => str.IsMatch(s));
5. Matchs() //正则：获取匹配的字符串
    Assert.Equal(new List<string> { "1", "2", "3" }, "1a2b3c".Matchs(@"\d+"));
6. MatchGroup() //正则：按组匹配
    Assert.Equal(new List<string> { "41272419880522659X", "412724", "19880522", "659X" }, "41272419880522659X@@@".MatchGroup(@"(\d{6})(\d{8})(\w{4})"));
7. MatchGroupFirst() //正则：正则匹配-组中第一个标记元素(括号标记):如a(\d+)b中的(\d+)
    Assert.Equal("123", "a123b".MatchGroupFirst(@"a(\d+)b"));
8. ReplaceRegex() //正则：替换
     Assert.Equal("abc", "abc".ReplaceRegex(@"\d+", @"88"));
9. BetweenRegex() //正则：获取中间字符串
    Assert.Equal("abc", "123abc456".BetweenRegex(@"\d{3}", @"\d{3}"));
10. T.ToJson() //实体转Json字符串
11. string.ToEntity<T>() //Json字符串转实体
12. JsonValue() //解析json字符串中的值
    Assert.Equal("a", "{\"name\":\"a\",\"value\":\"1\"}".JsonValue("name"));//普通对象
    Assert.Equal("666", "{\"name\":\"a\",\"value\":{\"name\":\"666\",\"value\":\"1\"}}".JsonValue("value.name"));//嵌套对象
13. FirstToUpper() //首字符大写
14. FirstToLower() //首字符小写
15. FormatDate() //格式化字符串样式日期
    Assert.Equal("20880808080800", "2088/08/08 08:08".FormatDate("yyyyMMddHHmmss"));
16. TimestampToTime() //时间戳字符串（毫秒级）转时间
    Assert.Equal(Convert.ToDateTime("1970/01/01T00:00:00"), "0".TimestampToTime())
17. TimestampSecondsToTime() //时间戳字符串（秒级）转时间
    Assert.Equal(Convert.ToDateTime("1970-01-01T00:00:00"), "0".TimestampSecondsToTime())
18. MaxLength() //从开始位置截取，获取最大长度字符串(包括省略符)
    Assert.Equal("北...", "北京多年来坚持实行".MaxLength(4));
19. Hidden() //隐藏部分
    Assert.Equal("188****8888", "18888888888".Hidden(3, 4));
20. Substring() //定关键词截取，前闭后开，如[123) > 12
    Assert.Equal("234", "0123456".Substring("2", "5"));
21. SplitReserveSeparator() //分割字符串，且保留分割词
    Assert.Equal(new List<string> { "0", ",", "1" }, "0,1".SplitReserveSeparator(","));
22. ParseCronExpression() //解析Cron表达式为中文表述
    Assert.Equal("每30秒", "0/30 * * * * ?".ParseCronExpression());
    Assert.Equal("每天1:00", "0 0 1 * * ?".ParseCronExpression());
    Assert.Equal("周一至周五里的每天10:15", "0 15 10 * ? mon-fri".ParseCronExpression());
```
6.  DateTimeExtension

``` csharp
1. TimeElapsed() //过了多久
    Assert.Equal("1分钟前", DateTime.Now.AddSeconds(-61).TimeElapsed(TimeScale.min));
    Assert.Equal("1天前", DateTime.Now.AddDays(-1).TimeElapsed(TimeScale.month));
2. UnixTimestampSecond() //获取Timestamp时间戳：秒级
2. UnixTimestampMillisecond() //获取Timestamp时间戳：毫秒级
3. IsOverlap() //日期范围是否有重合
    var rang41 = (Convert.ToDateTime("2000/01/01 00:00:00"), Convert.ToDateTime("2001/12/12 23:59:59"));
    var rang42 = (Convert.ToDateTime("1999/09/01 00:00:00"), Convert.ToDateTime("2002/12/12 00:00:00"));
    Assert.True(rang41.IsOverlap(rang42));
```
7.  DataTableHelper

``` csharp
1. List.ToDataTable() //将泛型集合类转换成DataTable
    DataTable ToDataTable<T>(this IList<T> list, string tableName = null)
2. DataTable.ToList() //将DataTable转为集合
    List<T> ToList<T>(this DataTable dt)
```
8.  Office：TableHelper

``` csharp
1. ConvertToEntity() //List<List<string>>字符串集合转实体
    List<T> ConvertToEntity<T>(List<List<string>> sourceDatas, List<MapInfo> columnInfos, int startRowIndex = 0)
```
9.  Security：ByteHelper、HashHelper、SecretHelper

``` csharp
Hash
1. MD5_16() //16位MD5加密-小写
2. MD5_32() //32位MD5加密-小写
3. Sha1() //Sha1算法-小写40位
4. Sha256() //Sha256算法-小写64位
5. Sha512() //Sha512算法-小写128位
6. ConvertToHexString() //字节数组转16进制字符串
```

#### 版本日志


1.0.0 完善基本功能
1.1.0 添加string扩展方法[cron表达式中文表述]：string.ParseCronExpression()
